package de.ostfalia.algodat.ss17;

/**
 * Created by EGAL on 20.03.2017.
 */
public class Paare {
    private String start;
    private String end;

    public Paare(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Paare{" +
                "start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}

package de.ostfalia.algodat.ss17;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by EGAL on 20.03.2017.
 */
public class Grammelot {
    private String wort;

    private String[] inp;
    private Paare[] wortpaar;


    public Grammelot(String wort) throws IOException {
        this.wort = wort;
        inp = new String[laenge()];
        einlesen();
        wortpaar = new Paare[inp.length];
        erstellePaare();
    }

    public String getWort() {
        return wort;
    }

    public void setWort(String wort) {
        this.wort = wort;
    }

    public String erzeuge(int n) {
        StringBuilder ret = new StringBuilder();
        String ende = ".";
        for (int i = 0; i < n; i++) {
            ende = zufaelligerNachfolger(ende);
            ret.append(ende).append(" ");
        }
        return ret.toString();
    }

    public String erzeuge(String start, int n) {

        return null;
    }

    public int anzahlMoeglicherNachfolger(String wort) {
        int zaehler = 0;

        for (int i = 0; i < wortpaar.length; i++) {
            if (wortpaar[i].getStart().equals(wort)) {
                zaehler++;
            }
        }
        return zaehler;
    }

    public String zufaelligerNachfolger (String wort) {
        int zaehler = 0;
        int anz = anzahlMoeglicherNachfolger(wort);

        if (anz == 0){
            return null;
        }



        for (int i = 0; i < wortpaar.length; i++) {
            if (wortpaar[i].getStart().equals(wort)) {
                zaehler++;
                if (zaehler == z) {
                    return wortpaar[i].getEnd();
                }
            }
        }

    }

    public String toString() {
        return null;
    }

    private int laenge() throws IOException {
        int length = 0;
        String data = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\CH405R4M80\\IdeaProjects\\AlgoDatSS17A1\\src\\Minimal.txt"));
            while ((data = br.readLine()) != null) {
                length++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("Datei nicht gefunden");
        } catch (NullPointerException e) {
            System.out.println("Fehlerhafte Datei");
        }
        return length;
    }

    private void einlesen() throws IOException {
        int nr = 0;
        String data = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\CH405R4M80\\IdeaProjects\\AlgoDatSS17A1\\src\\Minimal.txt"));
            while ((data = br.readLine()) != null) {
                inp[nr] = data;
                nr++;
                /** Just test..
                 *  System.out.println(inp[nr]);
                 */
            }
        } catch (FileNotFoundException e) {
            System.out.println("Datei nicht gefunden");
        } catch (NullPointerException e) {
            System.out.println("Fehlerhafte Datei");
        }
    }

    private void erstellePaare() {
        for (int i = 0; i < wortpaar.length; i++) {
            if (i == 0) {
                wortpaar[i] = new Paare(".", inp[i]);
            } else {
                wortpaar[i] = new Paare(inp[i - 1], inp[i]);
            }

            /** Just for test
             * System.out.println(wortpaar[i].getStart() + " + " + wortpaar[i].getEnd());
             */
        }
    }



}

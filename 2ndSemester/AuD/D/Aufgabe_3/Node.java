package de.ostfalia.algodat.ss17;

import java.util.ArrayList;
import java.util.List;


public class Node {
    public Node left;
    public Node right;
    public String value;
    public List<String> content = new ArrayList<>();

    /**
     * Construktor
     * @param value
     */
    public Node (String value) {
        this.value = value;
    }

    /**
     * Getter to return the value
     * @return return content
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter...
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }
}

package de.ostfalia.algodat.ss17;


import java.io.IOException;

/**
 * Created by Daniel Thümen on 20.03.2017.
 */
public class Main {


    public static void main(String[] args) throws IOException {
        Grammelot Minimal = new Grammelot("Minimal.txt");

        System.out.println("-----");

        System.out.println(Minimal.anzahlMoeglicherNachfolger("ueben"));
        System.out.println(Minimal.zufaelligerNachfolger("ueben"));
        System.out.println(Minimal.erzeuge(2));
        System.out.println(Minimal.erzeuge("Jeder", 2));
        //System.out.println(Minimal.folgendesElement("und"));
        //System.out.println(Minimal.linkesKind("Jeder"));

    }


}

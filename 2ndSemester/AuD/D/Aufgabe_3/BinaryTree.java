package de.ostfalia.algodat.ss17;

/**
 * Created by arch on 24/04/17.
 */
public class BinaryTree {
    private Node root;

    /**
     * Constructor...
     */
    public BinaryTree() {
        root = null;
    }

    /**
     * Insert content into the Binarytree
     * @param value the Value are the 2 words inside Paar
     */
    public void insert(Paar value) {
        if (root == null) {
            root = new Node(value.getStart());
            root.content.add(value.getEnd());
        } else {
            insert(root, value);
        }
    }

    /**
     * Recoursivly insert
     * @param parent parent is the Node above
     * @param value the content
     */
    public void insert (Node parent, Paar value) {
        if (parent.getValue().equals(value.getStart())) {
            parent.content.add(value.getEnd());
        } else if (parent.getValue().length() > value.getStart().length()) {
            if(parent.left == null) {
                parent.left = new Node(value.getStart());
                parent.left.content.add(value.getEnd());
            } else {
                    insert(parent.left, value);
            }
        } else {
            if (parent.right == null) {
                parent.right = new Node(value.getStart());
                parent.right.content.add(value.getEnd());
            } else {
                insert(parent.right, value);
            }
        }
    }

    /**
     * Search algorithm to search nodes
     * @param search search parameter
     * @return give the node
     */
    public Node searchNode (String search) {
        Node n = root;
        while (n != null && !(n.getValue().equals(search))) {
            if (search.length() < n.getValue().length()) {
                n = n.left;
            } else {
                n = n.right;
            }
        }
        return n;
    }



}

package de.ostfalia.algodat.ss17;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Daniel Thümen on 20.03.2017.
 */
public class Grammelot {
    private String wort;

    private String[] inp;
    private Paar[] wortpaar;
    private BinaryTree tree = new BinaryTree();

    /**
     * Konstruktor
     * @param wort
     * @throws IOException
     */
    public Grammelot(String wort) throws IOException {
        this.wort = wort;
        inp = new String[laenge()];
        einlesen();
        wortpaar = new Paar[inp.length];
        erstellePaar();
        //sort(0, wortpaar.length-1);


    }

    /**
     * Gibt namen aus
     * @return
     */
    public String getWort() {
        return wort;
    }


    /**
     * Setzt Namen
     * @param wort
     */
    public void setWort(String wort) {
        this.wort = wort;
    }


    /**
     * Erzeugt eine zufällige Wortkette
     * @param n
     * @return
     */
    public String erzeuge(int n) {
        return erzeuge(".", n).substring(2);
    }

    /**
     * Erzeugt eine Wortkette mit angegebenen Start
     * @param start
     * @param n
     * @return
     */
    public String erzeuge(String start, int n) {
        StringBuilder ret = new StringBuilder(start + " ");
        String ende = start;

        for (int i = 0; i < n; i++) {
            ende = zufaelligerNachfolger(ende);
            ret.append(ende).append(" ");
        }
        return ret.toString();
    }

    /**
     * Gibt anzahl möglicher Nachfolger an
     * @param wort
     * @return
     */
    public int anzahlMoeglicherNachfolger(String wort) {

        return tree.searchNode(wort).content.size();
    }

    /**
     * Sucht zufälligen Nacholger
     * @param wort Übergibt das zu Prüfende Wort
     * @return  Zufalls wort
     */
    public String zufaelligerNachfolger (String wort) {
        int anz = anzahlMoeglicherNachfolger(wort);

        if (anz == 0){
            return null;
        }

        int z = (int) (Math.random() * anz);
        return tree.searchNode(wort).content.get(z);

    }

    /**
     * Erzeugt die Länge für Array
     * @return  Länge vom Array
     * @throws IOException
     */
    private int laenge() throws IOException {
        int length = 0;
        String data = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("Minimal_C.txt"));
            while ((data = br.readLine()) != null) {
                length++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("Datei nicht gefunden");
        } catch (NullPointerException e) {
            System.out.println("Fehlerhafte Datei");
        }
        return length;
    }

    /**
     * List Array zur Tupel vorbereitung
     * @throws IOException
     */
    private void einlesen() throws IOException {
        int nr = 0;
        String data;
        try {
            BufferedReader br = new BufferedReader(new FileReader("Minimal_C.txt"));
            while ((data = br.readLine()) != null) {
                inp[nr] = data;
                nr++;
                /** Just test..
                 *  System.out.println(inp[nr]);
                 */
            }
        } catch (FileNotFoundException e) {
            System.out.println("Datei nicht gefunden");
        } catch (NullPointerException e) {
            System.out.println("Fehlerhafte Datei");
        }
    }

    /**
     * Erstellt Wortpaare als Tupel
     */
    private void erstellePaar() {
        for (int i = 0; i < wortpaar.length; i++) {
            if (i == 0) {
                wortpaar[i] = new Paar(".", inp[i]);
            } else {
                wortpaar[i] = new Paar(inp[i - 1], inp[i]);
            }
            tree.insert(wortpaar[i]);

            /** Just for test
             * System.out.println(wortpaar[i].getStart() + " + " + wortpaar[i].getEnd());
             */
            //System.out.println(wortpaar[i].getStart() + " + " + wortpaar[i].getEnd());

        }
    }

    public String linkesKind(String wort) {
        Node ret = tree.searchNode(wort);
        return ret.left.getValue();
    }

    public String rechtesKind (String wort) {
        Node ret = tree.searchNode(wort);
        return ret.right.getValue();
    }



    @Override
    public String toString() {
        return Arrays.toString(wortpaar);
    }
}

package de.ostfalia.algodat.ss17;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Daniel Thümen on 20.03.2017.
 */
public class Grammelot {
    private String wort;

    private String[] inp;
    private Paar[] wortpaar;

    /**
     * Konstruktor
     * @param wort
     * @throws IOException
     */
    public Grammelot(String wort) throws IOException {
        this.wort = wort;
        inp = new String[laenge()];
        einlesen();
        wortpaar = new Paar[inp.length];
        erstellePaar();
        sort(0, wortpaar.length-1);

        for (int i = 0; i < wortpaar.length; i++) {
            System.out.println(wortpaar[i]);
        }

    }

    /**
     * Gibt namen aus
     * @return
     */
    public String getWort() {
        return wort;
    }


    /**
     * Setzt Namen
     * @param wort
     */
    public void setWort(String wort) {
        this.wort = wort;
    }


    /**
     * Erzeugt eine zufällige Wortkette
     * @param n
     * @return
     */
    public String erzeuge(int n) {
        return erzeuge(".", n).substring(2);
    }

    /**
     * Erzeugt eine Wortkette mit angegebenen Start
     * @param start
     * @param n
     * @return
     */
    public String erzeuge(String start, int n) {
        StringBuilder ret = new StringBuilder(start + " ");
        String ende = start;

        for (int i = 0; i < n; i++) {
            ende = zufaelligerNachfolger(ende);
            ret.append(ende).append(" ");
        }
        return ret.toString();
    }

    /**
     * Gibt anzahl möglicher Nachfolger an
     * @param wort
     * @return
     */
    public int anzahlMoeglicherNachfolger(String wort) {
        return searchLast(wort) - searchFirst(wort);
    }

    /**
     * Sucht zufälligen Nacholger
     * @param wort Übergibt das zu Prüfende Wort
     * @return  Zufalls wort
     */
    public String zufaelligerNachfolger (String wort) {
        int zaehler = 0;
        int anz = anzahlMoeglicherNachfolger(wort);

        if (anz == 0){
            return null;
        }

        Paar[] rueckgabe = new Paar[anz];

        int middle, left = 0, right = wortpaar.length - 1;

        while (left <= right) {
            middle = (left + right) / 2;
            if (wortpaar[middle].getStart().compareTo(wort) == 0) {
                left++;
                rueckgabe[zaehler] = wortpaar[middle];
                zaehler++;
            } else if (wortpaar[middle].getStart().compareTo(wort) < 0) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }





        int z = (int) (Math.random() * anz);
        return rueckgabe[z].getEnd();

    }


    private int searchFirst(String wort) {
        int left = 0;
        int right = wortpaar.length;
        while (left < right) {
            int middle = (left + right) / 2;
            if (wortpaar[middle].getStart().compareTo(wort) < 0) {
                left = middle + 1;
            } else {
                right = middle;
            }
        }
        if (wortpaar[left].getStart().equals(wort)) {
            return left;
        } else {
            return -1;
        }
    }

    private int searchLast(String wort) {
        int left = 0;
        int right = wortpaar.length - 1;
        int ret = -1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (wortpaar[middle].getStart().compareTo(wort) == 0) {
                System.out.println(wort + "**");
                ret = middle;
                left = middle + 1;
            } else if (wortpaar[middle].getStart().compareTo(wort) < 0) {
                System.out.println(wort + "--");
                left = middle + 1;
            } else {
                System.out.println(wort + "++");
                right = middle - 1;
            }
        }
        if (wortpaar[ret].getStart().compareTo(wort) == 0) {
            return ret;
        } else {
            return 0;
        }
    }

    /**
     * Erzeugt die Länge für Array
     * @return  Länge vom Array
     * @throws IOException
     */
    private int laenge() throws IOException {
        int length = 0;
        String data = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("Minimal_C.txt"));
            while ((data = br.readLine()) != null) {
                length++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("Datei nicht gefunden");
        } catch (NullPointerException e) {
            System.out.println("Fehlerhafte Datei");
        }
        return length;
    }

    /**
     * List Array zur Tupel vorbereitung
     * @throws IOException
     */
    private void einlesen() throws IOException {
        int nr = 0;
        String data;
        try {
            BufferedReader br = new BufferedReader(new FileReader("Minimal_C.txt"));
            while ((data = br.readLine()) != null) {
                inp[nr] = data;
                nr++;
                /** Just test..
                 *  System.out.println(inp[nr]);
                 */
            }
        } catch (FileNotFoundException e) {
            System.out.println("Datei nicht gefunden");
        } catch (NullPointerException e) {
            System.out.println("Fehlerhafte Datei");
        }
    }

    /**
     * Erstellt Wortpaare als Tupel
     */
    private void erstellePaar() {
        for (int i = 0; i < wortpaar.length; i++) {
            if (i == 0) {
                wortpaar[i] = new Paar(".", inp[i]);
            } else {
                wortpaar[i] = new Paar(inp[i - 1], inp[i]);
            }

            /** Just for test
             * System.out.println(wortpaar[i].getStart() + " + " + wortpaar[i].getEnd());
             */
            //System.out.println(wortpaar[i].getStart() + " + " + wortpaar[i].getEnd());

        }
    }

    /**
     * Sortiert die Wortkette
     */
    private void sort(int low, int high) {
        int l = low, h = high;

        if (h - l >= 1) {
            String pivot = wortpaar[l].getStart().toLowerCase();
            while (h > l) {
                while (wortpaar[l].getStart().toLowerCase().compareTo(pivot) <= 0 && l < high && h > l) {
                    l++;
                }

                while (wortpaar[h].getStart().toLowerCase().compareTo(pivot) >= 0 && h > low && h >= l) {
                    h--;
                }

                if (h > l) {
                    Paar tmp = new Paar(wortpaar[l].getStart(), wortpaar[l].getEnd());
                    wortpaar[l] = new Paar(wortpaar[h].getStart(), wortpaar[h].getEnd());
                    wortpaar[h] = new Paar(tmp.getStart(), tmp.getEnd());
                }
            }

            // Tausche Pivot Element
            Paar tmp = new Paar(wortpaar[low].getStart(), wortpaar[low].getEnd());
            wortpaar[low] = new Paar(wortpaar[h].getStart(), wortpaar[h].getEnd());
            wortpaar[h] = new Paar(tmp.getStart(), tmp.getEnd());

            sort(low, h - 1);
            sort(h + 1, high);
        }



        /** Just for test
         *
         * for (int i = 0; i < wortpaar.length; i++) {
         *     System.out.println(wortpaar[i].getStart() + " - " + wortpaar[i].getEnd());
         * }
         */

    }

    /**
     * Gibt den nächsten schlüssel zurück
     * @param wort Such Parameter
     * @return Gibt den nächsten Schlüssel falls vorhanden zurück
     */
    public String folgendesElement(String wort) {
        try {
            return wortpaar[searchLast(wort) + 1].getStart();
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }

    }

    /**
     * Gibt den vorherigen Schlüssel zurück
     * @param wort such Parameter
     * @return Gibt den vorherigen Schlüssel zurück, falls vorhanden
     */
    public String vorherigesElement(String wort) {
        try {
            return wortpaar[searchFirst(wort) - 1].getStart();
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }

    }



    @Override
    public String toString() {
        return Arrays.toString(wortpaar);
    }
}

package de.ostfalia.prog.ss17;

import java.io.IOException;

/**
 * @author D. Thümen, A. Schernich
 *
 */
public class Spiel implements ISpiel {

    private int amZug;

    //TODO Attribute für Objekt Spieler
    private Farbe[] spieler;
    private int[] pos;


    /**
     * Konstruktor initialisiert die Liste der Spieler bzw. Spielfiguren
     * auf einem Standard Gänsespiel -Spielbrett (laut Aufgabebeschreibung).
     * Die Reihenfolge der Farbe entspricht der Reihenfolge wie die
     * Figuren gespielt werden. Die Figuren werden darüber hinaus
     * standardmäßig auf Position 0 des Spielfeldes platziert.
     *
     * @param farben Array mit Farben.
     */
    public Spiel(Farbe[] farben) {
        // toDo auf doppelten Farben ueberpruefen
        // Todo Anzahl spieler

        this.spieler = new Farbe[farben.length];
        this.pos = new int[farben.length];
        for (int i = 0; i < farben.length; i++) {
            spieler[i] = farben[i];
            pos[i] = 0;
        }
        //this.amZug = (int)(Math.random() * farben.length);
        this.amZug = 0;
        //System.out.println(this.amZug);
    }

    /**
     * Konstruktor initialisiert die Liste der Spieler bzw. Spielfiguren
     * samt deren Anfangsposition.
     * Die Reihenfolge der Farbe entspricht der Reihenfolge wie die
     * Figuren gespielt werden.
     * Darüber hinaus werden die Figuren auf die Position gebracht, die
     * aus dem String hervorgeht
     *
     * @param strFarbePosition Kommagetrennter String mit dem Format
     *                         "Position:Farbe" (z.B. "0:GELB, 10:ROT, 1:BLAU").
     */
    public Spiel(String strFarbePosition) {
        String[] tmp = strFarbePosition.split(", ");
        String[] einzelSpieler;
        this.spieler = new Farbe[tmp.length];
        this.pos = new int[tmp.length];
        for (int i = 0; i < tmp.length; i++) {
            einzelSpieler = tmp[i].split(":");
            this.pos[i] = Integer.parseInt(einzelSpieler[0]);
            switch (einzelSpieler[1]) {
                case "BLAU":
                    this.spieler[i] = Farbe.BLAU;
                    break;
                case "GELB":
                    this.spieler[i] = Farbe.GELB;
                    break;
                case "GRUEN":
                    this.spieler[i] = Farbe.GRUEN;
                    break;
                case "ROT":
                    this.spieler[i] = Farbe.ROT;
                    break;
                case "SCHWARZ":
                    this.spieler[i] = Farbe.SCHWARZ;
                    break;
                case "WEISS":
                    this.spieler[i] = Farbe.WEISS;
                    break;
                default:
                    break;
            }
        }

    }


    /**
     * Funktion:<br>
     * Fuehrt Move aus:<br>
     * Bewegt den Spieler und prueft, ob sich dieser auf Sonderfeld ist.<br>
     * Parameter:<br>
     * @param augenzahl - Uebergebenes Wuerfelergebnis
     * 
     */
    public void move(int augenzahl) {
        int tmp = this.pos[amZug] + augenzahl;
        boolean ereignis;

        do {
            ereignis = false;
            if (tmp >= 63) {
                if (tmp == 63) {
                    System.out.println("Spieler " + this.spieler[amZug] + "\n" +
                            " hat gewonnen");
                } else {
                    tmp = 63 - augenzahl;
                    ereignis = true;
                }
            }

            switch (tmp) {
                case 14:
                case 18:
                case 27:
                case 32:
                case 36:
                case 50:
                case 59:
                    tmp += augenzahl;
                    ereignis = true;
                    break;
                case 6:
                    tmp = 12;
                    ereignis = true;
                    break;
                case 42:
                    tmp = 30;
                    ereignis = true;
                    break;
                case 58:
                    tmp = 0;
                    ereignis = true;
                    break;
                default:
                    break;
            }
        } while (ereignis);

        this.pos[amZug] = tmp;

        amZug = (amZug + 1) % spieler.length;
    }

    /**
     * Getter, um Spieler am Zug zu liefern.
     * @return gibt farbe zurück
     */
    public Farbe getFigurAmZug() {
        return this.spieler[amZug];
    }


    /**
     * Getter, um die Spielfigur auf finalem Feld zu ermitteln.
     * @return - this.spieler (Gewinner)
     */
    public Farbe getGewinnerFigur() {
        for (int i = 0; i < pos.length; i++) {
            if (this.pos[i] == 63) {
                return this.spieler[i];
            }
        }
        return null;
    }

    /**
     * Funktion:<br>
     * Gibt Position von Spieler mit uebergebenr Farbe zurueck.
     * @param farbe gibt spielerfarbe in Methode
     * @return - pos[i] (Falls vorhanden), sonst -1 (Testentsprechend)
     */
    public int getFeldNummer(Farbe farbe) {
        for (int i = 0; i < spieler.length; i++) {
            if (spieler[i] == farbe) {
                return pos[i];
            }
        }
        return -1;
    }

    /**
     * todo:<br>
     * Speichert Spielstand von .txt.
     * @param dateiName - übergibt dateiName
     * @throws IOException - wirft fehler
     */
    public void speichern(String dateiName) throws IOException {

    }

    /**
     * ToString 
     */
    @Override
    public String toString() {
        return pos[amZug] + ":" + spieler[amZug];
    }

}

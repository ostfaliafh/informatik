package de.ostfalia.prog.ss17;

/**
 * @author D. Thümen, A. Schernich
 *
 */

/**
 * enum Farbe definiert die 6 mögliche Farben für die Figuren des
 * Gänsespiels. Jede Figur darf nur eine Farbe haben und eine Farbe darf
 * nur einer Figur zugeordnet werden (1:1 Beziehung).
 */
public enum Farbe {
    BLAU, GELB, GRUEN, ROT, SCHWARZ, WEISS;
}

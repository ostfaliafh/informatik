package de.ostfalia.prog.ss17;

import java.io.IOException;

/**
 * @author D. Thümen, A. Schernich
 *
 */

public interface ISpiel {
    /**
     * Bewegt die Spielfigur, die am Zug ist, sofern sie nicht aussetzen muss.
     *
     * @param augenzahl Die Anzahl Felder, die die Figur, welche am Zug ist,
     *                  erstmals vorrücken muss. Die aktuelle Position der Figur und „augenzahl“
     *                  ergeben eine Zielposition. Befindet sich auf ihr ein Standardfeld, wird
     *                  die Figur einfach zusammen mit den evt. schon vorhandenen Figuren darauf
     *                  platziert. Befindet sich auf der Zielposition ein Ereignisfeld, wird sie,
     *                  so wie das Ereignis verlangt, korrigiert. Z.B. Figur befindet sich auf
     *                  Feld Nr. 0 und würfelt die Zahlen 5 und 1. Somit soll die Figur 6 Felder
     *                  vorrücken. Auf der Zielposition (Feld Nr. 6) ist allerdings ein Ereignisfeld,
     *                  das besagt, Figur soll weiter auf Feld Nr. 12 ziehen. Die Zielposition
     *                  muss entsprechend korrigiert werden damit die Figur tatsächlich auf dem
     *                  Feld Nr. 12 positioniert werden kann.
     */

    public void move(int augenzahl);

    /**
     * Liefert die Figur, die aktuell am Zug ist.
     *
     * @return Die Figur, die aktuell am Zug ist (repräsentiert anhand ihrer Farbe)
     */
    public Farbe getFigurAmZug();

    /**
     * Liefert die Figur, die als erste das Zielfeld erreicht hat.
     *
     * @return Die Figur, die das Zielfeld erreicht hat (repräsentiert anhand ihrer
     * Farbe), sonst null
     */
    public Farbe getGewinnerFigur();

    /**
     * Liefert die Nummer des Feldes, wo die gesuchte Figur sich befindet
     *
     * @param farbe Die gesuchte Figur, welche anhand ihrer Farbe repräsentiert wird
     * @return die Nummer des Feldes (0 ... 63), wo die gesuchte Figur sich befindet,
     * sonst -1, wenn die Figur sich nicht auf dem Spielfeld befindet
     */
    public int getFeldNummer(Farbe farbe);

    /**
     * Methode speichert den aktuellen Zustand des Gänsespiels in einer Textdatei.
     * Dabei werden gespeichert: die Figuren selbst, die Position wo sie sich auf dem
     * Spielfeld befinden und die Reihenfolge, wie sie gespielt werden.
     *
     * @param dateiName Der Name der Datei
     */
    public void speichern(String dateiName) throws IOException;

    /**
     * Liefert ein String mit der Abbildung des Spielfeldes zurück. Bitte nur ASCII
     * Zeichen verwenden.
     *
     * @return Ein String mit der Abbildung des Spielfeldes
     */
    @Override
    public String toString();
}

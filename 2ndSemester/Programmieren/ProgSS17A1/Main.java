package de.ostfalia.prog.ss17;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @author D. Thümen, A. Schernich
 *
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        /**
         * Farbe[] farben = {Farbe.BLAU, Farbe.GELB, Farbe.GRUEN, Farbe.ROT, Farbe.SCHWARZ, Farbe.WEISS};
         * Spiel test = new Spiel(farben);
         * System.out.println(test.getFigurAmZug());
         * System.out.println(test.getFeldNummer(Farbe.ROT));
         * test.move(5);
         * System.out.println(test.getFeldNummer(Farbe.ROT));
         * System.out.println(test.getFeldNummer(Farbe.GELB));
         * System.out.println(test.getFeldNummer(Farbe.GRUEN));
         * System.out.println(test.getFeldNummer(Farbe.BLAU));
         * System.out.println(test.getFeldNummer(Farbe.SCHWARZ));
         * System.out.println(test.getFeldNummer(Farbe.WEISS));
         * System.out.println(test.toString());
        */
        Scanner sc = new Scanner(System.in);
        List<String> farbwahl = new ArrayList<String>();
        farbwahl.add("BLAU");
        farbwahl.add("GELB");
        farbwahl.add("GRUEN");
        farbwahl.add("ROT");
        farbwahl.add("SCHWARZ");
        farbwahl.add("WEISS");

        Farbe[] farben;
        int inp;


        System.out.println("Wie viele Spieler gibt es?");
        inp = sc.nextInt();
        farben = new Farbe[inp];
        System.out.println("Bitte Farbe Waehlen");
        for (int i = 0; i < farben.length; i++) {
            System.out.println("Spieler " + (i + 1));
            for (int j = 0; j < farbwahl.size(); j++) {
                System.out.println(j + ". " + farbwahl.get(j));
            }
            inp = sc.nextInt();
            switch (farbwahl.get(inp)) {
                case "BLAU":
                    farben[i] = Farbe.BLAU;
                    farbwahl.remove("BLAU");
                    break;
                case "GELB":
                    farben[i] = Farbe.GELB;
                    farbwahl.remove("GELB");
                    break;
                case "GRUEN":
                    farben[i] = Farbe.GRUEN;
                    farbwahl.remove("GRUEN");
                    break;
                case "ROT":
                    farben[i] = Farbe.ROT;
                    farbwahl.remove("ROT");
                    break;
                case "SCHWARZ":
                    farben[i] = Farbe.SCHWARZ;
                    farbwahl.remove("SCHWARZ");
                    break;
                case "WEISS":
                    farben[i] = Farbe.WEISS;
                    farbwahl.remove("WEISS");
                    break;
            }
        }

        Spiel run = new Spiel(farben);
        boolean running = true;
        int wuerfel;

        do {
            if (run.getFeldNummer(run.getFigurAmZug()) == 63) {
                System.out.println("Der Gewinner ist: " + run.getGewinnerFigur());
                running = false;
            } else {
                wuerfel = (int)(Math.random() * 6 + 1) + (int)(Math.random() * 6 + 1);
                System.out.println("Spieler " + run.getFigurAmZug() + " ist am Zug\n" +
                        "Du befindest dich auf Feld: " + run.getFeldNummer(run.getFigurAmZug()) + "\n" +
                        "Du hast gewuerfelt: " + wuerfel);
                run.move(wuerfel);
            }
        } while (running == true);
    }
}

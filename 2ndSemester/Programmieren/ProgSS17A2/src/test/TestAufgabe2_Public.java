package de.ostfalia.prog.ss17.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.StringJoiner;

import org.junit.Before;
import org.junit.Test;

import de.ostfalia.prog.ss17.FalscheFigurenException;
import de.ostfalia.prog.ss17.Farbe;
import de.ostfalia.prog.ss17.ISpiel;
import de.ostfalia.prog.ss17.Spiel;

public class TestAufgabe2_Public {

	@Before
	public void setUp() throws Exception {}

	
	@Test (timeout=100)
	public void testGeheZurueck() throws FalscheFigurenException {
		Farbe[] figuren = {Farbe.BLAU, Farbe.GELB};
		ISpiel spiel = new Spiel("3:BLAU, 3:GELB");
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", 
				     "[3:BLAU, 3:GELB]", stellung(spiel, figuren));
		
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 
				      spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 5 und muss zurueck auf Feld 3.", 
				     "[3:BLAU, 3:GELB]", stellung(spiel, figuren));		
	}
	
	
	@Test (timeout=100)
	public void testNochmalWuerfeln() throws FalscheFigurenException {
		Farbe[] figuren = {Farbe.BLAU, Farbe.GELB};
		ISpiel spiel = new Spiel("26:BLAU, 28:GELB");		
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", 
				     "[26:BLAU, 28:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 				
				      spiel.getFigurAmZug());		
		spiel.move(5);		
		assertEquals("BLAU zieht auf Feld 31.", 
				     "[31:BLAU, 28:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		
		assertEquals("BLAU steht auf Feld 31 und darf nochmal wuerfeln.", 
				     Farbe.BLAU, spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 33.", 
			         "[33:BLAU, 28:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		
		assertEquals("Naechster am Zug ist GELB", Farbe.GELB, 				
			         spiel.getFigurAmZug());
		spiel.move(6);
		assertEquals("BLAU zieht auf Feld 34.", 
			         "[33:BLAU, 34:GELB]", stellung(spiel, figuren));
	}
	
	@Test (timeout=100)
	public void testAussetzen() throws FalscheFigurenException {
		Farbe[] figuren = {Farbe.BLAU, Farbe.GELB};
		ISpiel spiel = new Spiel("15:BLAU, 20:GELB");		
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", 
				     "[15:BLAU, 20:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 				
				      spiel.getFigurAmZug());		
		spiel.move(4);		
		assertEquals("BLAU zieht auf Feld 19 und muss einmal aussetzen.", 
				     "[19:BLAU, 20:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		
		assertEquals("GELB ist als naechtster am Zug.", 
				     Farbe.GELB, spiel.getFigurAmZug());
		spiel.move(4);
		assertEquals("GELB zieht auf Feld 24.", 
			         "[19:BLAU, 24:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		
		assertEquals("BLAU steht auf 19 und muss aussetzen. GELB ist am Zug", 
				     Farbe.GELB, spiel.getFigurAmZug());
		spiel.move(4);
		assertEquals("GELB zieht auf Feld 28.", 
			         "[19:BLAU, 28:GELB]", stellung(spiel, figuren));
		//-------------------------------------------------------
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 				
			      	  spiel.getFigurAmZug());		
		spiel.move(3);		
		assertEquals("BLAU zieht auf Feld 22.", 
				     "[22:BLAU, 28:GELB]", stellung(spiel, figuren));
	}
	
	@Test (timeout=100)
	public void testGlueckImLeben() throws FalscheFigurenException {
		Farbe[] figuren = {Farbe.BLAU, Farbe.GELB};
		ISpiel spiel = new Spiel("40:BLAU, 46:GELB");		
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", 
				     "[40:BLAU, 46:GELB]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 
				      spiel.getFigurAmZug());
		
		//------------------------------------------------------
		spiel.move(12);		
		assertEquals("BLAU zieht auf Feld 52 und darf noch einmal wuerfeln.", 
				     "[52:BLAU, 46:GELB]", stellung(spiel, figuren));
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 
			         spiel.getFigurAmZug());
		
		//------------------------------------------------------
		spiel.move(3);
		assertEquals("BLAU zieht auf Feld 55.", 
			         "[55:BLAU, 46:GELB]", stellung(spiel, figuren));
		assertEquals("Naechster am Zug ist GELB", Farbe.GELB, 
		              spiel.getFigurAmZug());
		
		//------------------------------------------------------
		spiel.move(6);
		assertEquals("GELB zieht auf Feld 52.", 
			         "[55:BLAU, 52:GELB]", stellung(spiel, figuren));
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 
		              spiel.getFigurAmZug());
	}
	
	@Test (timeout=100)
	public void testPingPong() throws FalscheFigurenException {
		Farbe[] figuren = {Farbe.BLAU, Farbe.GELB};
		ISpiel spiel = new Spiel("13:BLAU, 12:GELB");
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", 
				     "[13:BLAU, 12:GELB]", stellung(spiel, figuren));
		
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 
				      spiel.getFigurAmZug());
		
		spiel.move(5);
		assertEquals("BLAU zieht auf Feld 18 und muss weiter auf Feld 23.", 
				     "[23:BLAU, 12:GELB]", stellung(spiel, figuren));
		assertEquals("Naechster am Zug ist GELB", Farbe.GELB, 
		              spiel.getFigurAmZug());
		spiel.move(3);
		assertEquals("GELB zieht auf Feld 15.", 
			         "[23:BLAU, 15:GELB]", stellung(spiel, figuren));
		assertEquals("BLAU muss aussetzen. Naechster am Zug ist GELB", Farbe.GELB, 
	              spiel.getFigurAmZug());
		spiel.move(5);
		assertEquals("GELB zieht auf Feld 20.", 
		             "[23:BLAU, 20:GELB]", stellung(spiel, figuren));
	}
	
	@Test (timeout=100)
	public void testZugfolge() throws FalscheFigurenException {
		Zug[] sequenz = {
				new Zug( 1,    Farbe.BLAU,  9, "[0:BLAU, 0:ROT]"),
				new Zug( 2,     Farbe.ROT, 10, "[0:BLAU, 10:ROT]"),
				new Zug( 3,    Farbe.BLAU,  5, "[0:BLAU, 10:ROT]"),
				new Zug( 4,     Farbe.ROT,  9, "[0:BLAU, 19:ROT]"),
				new Zug( 5,    Farbe.BLAU, 10, "[10:BLAU, 19:ROT]"),
				new Zug( 6,    Farbe.BLAU,  8, "[26:BLAU, 19:ROT]"),
				new Zug( 7,     Farbe.ROT,  4, "[26:BLAU, 19:ROT]"),
				new Zug( 8,    Farbe.BLAU,  8, "[34:BLAU, 19:ROT]"),
				new Zug( 9,    Farbe.BLAU,  4, "[38:BLAU, 19:ROT]"),
				new Zug(10,     Farbe.ROT,  2, "[38:BLAU, 21:ROT]"),
				new Zug(11,    Farbe.BLAU, 12, "[62:BLAU, 21:ROT]"),
				new Zug(12,     Farbe.ROT,  6, "[62:BLAU, 33:ROT]"),
				new Zug(13,    Farbe.BLAU,  9, "[45:BLAU, 33:ROT]"),
				new Zug(14,     Farbe.ROT,  2, "[45:BLAU, 35:ROT]"),
				new Zug(15,     Farbe.ROT, 12, "[45:BLAU, 47:ROT]"),
				new Zug(16,    Farbe.BLAU,  5, "[55:BLAU, 47:ROT]"),
				new Zug(17,     Farbe.ROT, 12, "[55:BLAU, 51:ROT]"),
				new Zug(18,    Farbe.BLAU, 11, "[52:BLAU, 51:ROT]"),
				new Zug(19,     Farbe.ROT, 11, "[52:BLAU, 62:ROT]"),
				new Zug(20,    Farbe.BLAU,  7, "[56:BLAU, 62:ROT]"),
				new Zug(21,     Farbe.ROT,  3, "[56:BLAU, 60:ROT]"),
				new Zug(22,    Farbe.BLAU, 12, "[51:BLAU, 60:ROT]"),
				new Zug(23,     Farbe.ROT, 11, "[51:BLAU, 52:ROT]"),
				new Zug(24,    Farbe.BLAU,  5, "[56:BLAU, 52:ROT]"),
				new Zug(25,     Farbe.ROT,  9, "[56:BLAU, 61:ROT]"),
				new Zug(26,    Farbe.BLAU,  6, "[62:BLAU, 61:ROT]"),
				new Zug(27,     Farbe.ROT,  5, "[62:BLAU, 0:ROT]"),
				new Zug(28,    Farbe.BLAU,  5, "[0:BLAU, 0:ROT]"),
				new Zug(29,     Farbe.ROT, 10, "[0:BLAU, 10:ROT]"),
				new Zug(30,    Farbe.BLAU,  8, "[8:BLAU, 10:ROT]"),
				new Zug(31,     Farbe.ROT,  8, "[8:BLAU, 26:ROT]"),
				new Zug(32,    Farbe.BLAU,  3, "[11:BLAU, 26:ROT]"),
				new Zug(33,     Farbe.ROT,  3, "[11:BLAU, 29:ROT]"),
				new Zug(34,    Farbe.BLAU,  3, "[17:BLAU, 29:ROT]"),
				new Zug(35,     Farbe.ROT,  7, "[17:BLAU, 43:ROT]"),
				new Zug(36,    Farbe.BLAU,  3, "[20:BLAU, 43:ROT]"),
				new Zug(37,     Farbe.ROT,  4, "[20:BLAU, 47:ROT]"),
				new Zug(38,    Farbe.BLAU,  2, "[22:BLAU, 47:ROT]"),
				new Zug(39,     Farbe.ROT,  5, "[22:BLAU, 52:ROT]"),
				new Zug(40,    Farbe.BLAU,  2, "[24:BLAU, 52:ROT]"),
				new Zug(41,     Farbe.ROT, 12, "[24:BLAU, 51:ROT]"),
				new Zug(42,    Farbe.BLAU,  9, "[33:BLAU, 51:ROT]"),
				new Zug(43,     Farbe.ROT,  8, "[33:BLAU, 55:ROT]"),
				new Zug(44,    Farbe.BLAU,  2, "[35:BLAU, 55:ROT]"),
				new Zug(45,     Farbe.ROT, 10, "[35:BLAU, 53:ROT]"),
				new Zug(46,    Farbe.BLAU,  2, "[37:BLAU, 53:ROT]"),
				new Zug(47,     Farbe.ROT, 10, "[37:BLAU, 63:ROT]")
			};
		
		Farbe[] figuren = {Farbe.BLAU, Farbe.ROT};
		ISpiel spiel = new Spiel(figuren);
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", 
				     "[0:BLAU, 0:ROT]", stellung(spiel, figuren));
		
		evalSequenz(spiel, figuren, sequenz);
				
		assertEquals("ROT hat gewonnen.", Farbe.ROT, 
				     spiel.getGewinnerFigur());
	}
	
	@Test (timeout=100)
	public void testSpeichernLaden() throws IOException, FalscheFigurenException {
		String dateiname = "Spiel.txt";
		Farbe[] figuren = {Farbe.BLAU, Farbe.GELB};
		ISpiel spiel = new Spiel("2:BLAU, 3:GELB");	
		
		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 4.", 
				     "[4:BLAU, 3:GELB]", stellung(spiel, figuren));
		
		//-------------------------------------------------
		spiel.speichern(dateiname);
		
		spiel.move(4);
		assertEquals("GELB zieht auf Feld 7.", 
				     "[4:BLAU, 7:GELB]", stellung(spiel, figuren));
		
		//-------------------------------------------------
		spiel = Spiel.ladenSpiel(dateiname);
		assertEquals("Figurstellung nach dem Laden inkorrekt.", 
			         "[4:BLAU, 3:GELB]", stellung(spiel, figuren));
		assertEquals("Naechster am Zug ist GELB", Farbe.GELB, 
				      spiel.getFigurAmZug());
		spiel.move(3);
		assertEquals("GELB zieht auf Feld 6 und muss weiter nach 12.", 
		             "[4:BLAU, 12:GELB]", stellung(spiel, figuren));
		
		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, 
			      	 spiel.getFigurAmZug());
		spiel.move(5);
		assertEquals("BLAU zieht auf Feld 9 und muss zurÃƒÂ¼ck nach 4.", 
	                 "[4:BLAU, 12:GELB]", stellung(spiel, figuren));
	}
	
	//----------------------------------------------------------
	
	private String stellung(ISpiel spiel, Farbe... figuren) {
		StringJoiner sj = new StringJoiner(", ", "[", "]");
		for (Farbe farbe : figuren) {
			int pos = spiel.getFeldNummer(farbe);
			sj.add(pos + ":" + farbe);
		}
		return sj.toString();
	}
	
	
	private void evalSequenz(ISpiel spiel, Farbe[] figuren, Zug[] sequenz) {
		for (Zug zug : sequenz) {
			assertEquals(zug.num + ". Falsche Figur am Zug.", zug.amZug, 
					spiel.getFigurAmZug());
			spiel.move(zug.augenzahl);
			assertEquals(zug.num + ". Ungueltige Stellung.", zug.stellung, 
					stellung(spiel, figuren));
//			System.out.println(spiel);
		}
	}

	//----------------------------------------------------------
	
	private class Zug {
		
		private int num;
		private Farbe amZug;
		private int augenzahl;
		private String stellung;
		
		public Zug(int num, Farbe amZug, int augenzahl, String stellung) {
			this.num = num;
			this.amZug = amZug;
			this.augenzahl = augenzahl;
			this.stellung = stellung;
		}		
		
	}

}

package test;

import static org.junit.Assert.*;

import org.junit.Test;

import de.ostfalia.prog.ss17.Farbe;
import de.ostfalia.prog.ss17.ISpiel;
import de.ostfalia.prog.ss17.Spiel;

import org.junit.Before;

import java.lang.reflect.Method;
import java.util.StringJoiner;

public class Test1Public {

	@Before
	public void setUp() throws Exception {
	}

	@Test(timeout = 100)
	public void testGrundstellung() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel(figuren);
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[0:BLAU, 0:GELB]", stellung(spiel, figuren));

		Farbe[] unexp = { Farbe.GRUEN, Farbe.ROT, Farbe.WEISS, Farbe.SCHWARZ };
		assertEquals("Ungueltige Figuren auf dem Spielfeld.", "[-1:GRUEN, -1:ROT, -1:WEISS, -1:SCHWARZ]",
				stellung(spiel, unexp));
	}

	@Test(timeout = 100)
	public void testToString() throws NoSuchMethodException, SecurityException {
		Method m = Spiel.class.getMethod("toString", new Class[0]);
		assertNotEquals("Methode toString() darf nicht von Object stammen.", Object.class, m.getDeclaringClass());

		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel(figuren);
		String res = spiel.toString();
		assertNotNull("Methode toString() darf nicht null liefern", res);
		assertTrue("Methode toString() darf keinen leeren String liefern", res.length() > 0);
	}

	@Test(timeout = 100)
	public void testZieheEineFigur() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel(figuren);
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[0:BLAU, 0:GELB]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist BLAU.", Farbe.BLAU, spiel.getFigurAmZug());

		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 2.", "[2:BLAU, 0:GELB]", stellung(spiel, figuren));
	}

	@Test(timeout = 100)
	public void testZieheAufGleichePosition() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel(figuren);
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[0:BLAU, 0:GELB]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist BLAU.", Farbe.BLAU, spiel.getFigurAmZug());

		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 2.", "[2:BLAU, 0:GELB]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist GELB.", Farbe.GELB, spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("GELB zieht auf Feld 2.", "[2:BLAU, 2:GELB]", stellung(spiel, figuren));
	}

	@Test(timeout = 100)
	public void testGewonnen() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel("61:BLAU, 62:GELB");
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[61:BLAU, 62:GELB]", stellung(spiel, figuren));
		assertEquals("Es hat noch keiner gewonnen.", null, spiel.getGewinnerFigur());

		assertEquals("Naechster am Zug ist BLAU.", Farbe.BLAU, spiel.getFigurAmZug());

		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 63.", "[63:BLAU, 62:GELB]", stellung(spiel, figuren));
		assertEquals("BLAU hat gewonnen.", Farbe.BLAU, spiel.getGewinnerFigur());
	}

	@Test(timeout = 100)
	public void testBruecke() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel("0:BLAU, 0:GELB");
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[0:BLAU, 0:GELB]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist BLAU.", Farbe.BLAU, spiel.getFigurAmZug());

		spiel.move(6);
		assertEquals("BLAU zieht auf Feld 6 und darf weiter auf Feld 12.", "[12:BLAU, 0:GELB]",
				stellung(spiel, figuren));
	}

	@Test(timeout = 100)
	public void testZurueckZumAnfang() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel = new Spiel("53:BLAU, 40:GELB");
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[53:BLAU, 40:GELB]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist BLAU.", Farbe.BLAU, spiel.getFigurAmZug());

		spiel.move(5);
		assertEquals("BLAU zieht auf Feld 58 und muss zurueck zum Anfang.", "[0:BLAU, 40:GELB]",
				stellung(spiel, figuren));
		assertEquals("Naechster am Zug ist GELB", Farbe.GELB, spiel.getFigurAmZug());
	}

	@Test(timeout = 100)
	public void testGeheWeiter() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB, Farbe.ROT, Farbe.GRUEN, Farbe.WEISS, Farbe.SCHWARZ };
		ISpiel spiel = new Spiel("12:BLAU, 15:GELB, 25:ROT, 30:GRUEN, 34:WEISS, 47:SCHWARZ");
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.",
				"[12:BLAU, 15:GELB, 25:ROT, 30:GRUEN, 34:WEISS, 47:SCHWARZ]", stellung(spiel, figuren));

		assertEquals("Naechster am Zug ist BLAU", Farbe.BLAU, spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("BLAU zieht auf Feld 14 und muss weiter auf Feld 16.",
				"[16:BLAU, 15:GELB, 25:ROT, 30:GRUEN, 34:WEISS, 47:SCHWARZ]", stellung(spiel, figuren));
		// ---------------------------------------------------------------
		assertEquals("Naechster am Zug ist GELB", Farbe.GELB, spiel.getFigurAmZug());
		spiel.move(3);
		assertEquals("GELB zieht auf Feld 18 und muss weiter auf Feld 21.",
				"[16:BLAU, 21:GELB, 25:ROT, 30:GRUEN, 34:WEISS, 47:SCHWARZ]", stellung(spiel, figuren));
		// ---------------------------------------------------------------
		assertEquals("Naechster am Zug ist ROT", Farbe.ROT, spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("ROT zieht auf Feld 27 und muss weiter auf Feld 29.",
				"[16:BLAU, 21:GELB, 29:ROT, 30:GRUEN, 34:WEISS, 47:SCHWARZ]", stellung(spiel, figuren));
		// ---------------------------------------------------------------
		assertEquals("Naechster am Zug ist GRUEN", Farbe.GRUEN, spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("GRUEN zieht auf Feld 32 und muss weiter auf Feld 34.",
				"[16:BLAU, 21:GELB, 29:ROT, 34:GRUEN, 34:WEISS, 47:SCHWARZ]", stellung(spiel, figuren));
		// ---------------------------------------------------------------
		assertEquals("Naechster am Zug ist WEISS", Farbe.WEISS, spiel.getFigurAmZug());
		spiel.move(2);
		assertEquals("WEISS zieht auf Feld 36 und muss weiter auf Feld 38.",
				"[16:BLAU, 21:GELB, 29:ROT, 34:GRUEN, 38:WEISS, 47:SCHWARZ]", stellung(spiel, figuren));
		// ---------------------------------------------------------------
		assertEquals("Naechster am Zug ist SCHWARZ", Farbe.SCHWARZ, spiel.getFigurAmZug());
		spiel.move(3);
		assertEquals("SCHWARZ zieht auf Feld 50 und muss weiter auf Feld 53.",
				"[16:BLAU, 21:GELB, 29:ROT, 34:GRUEN, 38:WEISS, 53:SCHWARZ]", stellung(spiel, figuren));
	}

	@Test(timeout = 100)
	public void testZugfolge1() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.ROT };
		ISpiel spiel = new Spiel(figuren);
		assertEquals("Ausgangsstellung der Figuren nicht korrekt.", "[0:BLAU, 0:ROT]", stellung(spiel, figuren));
		Zug[] sequenz = { new Zug(1, Farbe.BLAU, 6, "[12:BLAU, 0:ROT]"), new Zug(2, Farbe.ROT, 4, "[12:BLAU, 4:ROT]"),
				new Zug(3, Farbe.BLAU, 12, "[24:BLAU, 4:ROT]"), new Zug(4, Farbe.ROT, 4, "[24:BLAU, 8:ROT]"),
				new Zug(5, Farbe.BLAU, 12, "[48:BLAU, 8:ROT]"), new Zug(6, Farbe.ROT, 4, "[48:BLAU, 12:ROT]"),
				new Zug(7, Farbe.BLAU, 11, "[52:BLAU, 12:ROT]"), new Zug(8, Farbe.ROT, 9, "[52:BLAU, 21:ROT]"),
				new Zug(9, Farbe.BLAU, 11, "[63:BLAU, 21:ROT]") };

		evalSequenz(spiel, figuren, sequenz);
		assertEquals("BLAU hat gewonnen.", Farbe.BLAU, spiel.getGewinnerFigur());
	}

	@Test(timeout = 100)
	public void testZweiSpiele() {
		Farbe[] figuren = { Farbe.BLAU, Farbe.GELB };
		ISpiel spiel1 = new Spiel("3:BLAU, 20:GELB");
		ISpiel spiel2 = new Spiel("40:GELB, 28:BLAU");

		assertEquals("Ausgangsstellung Spiel 1 nicht korrekt.", "[3:BLAU, 20:GELB]", stellung(spiel1, figuren));
		assertEquals("Ausgangsstellung Spiel 2 nicht korrekt.", "[28:BLAU, 40:GELB]", stellung(spiel2, figuren));

		assertEquals("Spiel 1, Naechster am Zug ist BLAU.", Farbe.BLAU, spiel1.getFigurAmZug());
		assertEquals("Spiel 2, Naechster am Zug ist GELB.", Farbe.GELB, spiel2.getFigurAmZug());

		spiel1.move(3);
		assertEquals("Spiel 1: BLAU zieht auf Feld 6 und muss weiter auf Feld 12.", "[12:BLAU, 20:GELB]",
				stellung(spiel1, figuren));
		spiel2.move(2);
		assertEquals("Spiel 2: GELB zieht auf Feld 42 und muss zurueck auf Feld 30.", "[28:BLAU, 30:GELB]",
				stellung(spiel2, figuren));
	}

	// ------------------------------------------

	private String stellung(ISpiel spiel, Farbe... figuren) {
		StringJoiner sj = new StringJoiner(", ", "[", "]");
		for (Farbe farbe : figuren) {
			int pos = spiel.getFeldNummer(farbe);
			sj.add(pos + ":" + farbe);
		}
		return sj.toString();
	}

	private void evalSequenz(ISpiel spiel, Farbe[] figuren, Zug[] sequenz) {
		for (Zug zug : sequenz) {
			assertEquals(zug.num + ". Falsche Figur am Zug.", zug.amZug, spiel.getFigurAmZug());
			spiel.move(zug.augenzahl);
			assertEquals(zug.num + ". Ungueltige Stellung.", zug.stellung, stellung(spiel, figuren));
			// System.out.println(spiel);
		}
	}

	// ----------------------------------------------------------

	private class Zug {

		private int num;
		private Farbe amZug;
		private int augenzahl;
		private String stellung;

		public Zug(int num, Farbe amZug, int augenzahl, String stellung) {
			this.num = num;
			this.amZug = amZug;
			this.augenzahl = augenzahl;
			this.stellung = stellung;
		}

	}

}
package de.ostfalia.prog.ss17;

/**
 * Ermöglicht einem Spieler unter bestimmten Bedingungen noch einmal zu würfeln.
 * 
 * @author Arne
 *
 */
public class WuerfelFeld extends Feld implements IEreignisFeld {

	private int gewuerfelt;

	/**
	 * Lässt den Spieler unter bestimmten Bedingungen noch einmal wuerfeln.
	 * 
	 * @param feldnummer
	 *            - Die Feldnummer.
	 * @param gewuerfelt
	 *            - Augenzahl, bei welcher der Spieler noch einmal wuerfeln
	 *            darf. Bei 0 wird angenommen, dass der Spieler unabhängig von
	 *            der Augenzahl noch einmal würfeln darf.
	 */
	public WuerfelFeld(int feldnummer, int gewuerfelt) {
		super(feldnummer);
		this.setGewuerfelt(gewuerfelt);
	}

	/**
	 * Laesst den Spieler noch einmal wuerfeln.
	 * 
	 * @param feldNummer
	 *            - Die Feldnummer.
	 */
	public WuerfelFeld(int feldNummer) {
		this(feldNummer, 0);
	}

	/**
	 * @return the gewuerfelt
	 */
	public int getGewuerfelt() {
		return gewuerfelt;
	}

	/**
	 * @param gewuerfelt
	 *            the gewuerfelt to set
	 */
	public void setGewuerfelt(int gewuerfelt) {
		this.gewuerfelt = gewuerfelt;
	}

	@Override
	public IEreignis setzeSpieler(Spieler spieler, int augenzahl) {
		super.setzeSpieler(spieler);
		
		if (this.gewuerfelt == augenzahl) {
			return new WuerfelEreignis();
		} else if (this.gewuerfelt == 0){
			return new WuerfelEreignis();
		}else {
			return new NichtsPassiertEreignis();
		}
	}

}
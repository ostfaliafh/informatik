package de.ostfalia.prog.ss17;

import java.io.IOException;

/**
 * Main.
 * @author - A. Schernich, D. Thümen
 */
public class Main {
	
	public static final int DREIUNDSECHZIG = 63;
/**
 * 
 * @param args - Standartargs
 * @throws InterruptedException - todo ausfuellen
 */
	public static void main(String[] args) throws InterruptedException, IOException {

//		Spiel run = new Spiel(UserInput.anfangsFragen());
		Spiel run = new Spiel("2:GELB, 10:ROT, 5:BLAU");

		boolean running = true;

//		run.speichern("test2");
//		run.ladenSpiel("test2.txt");

		do {
			
			// Falls Figur auf Gewinnerfeld -> Ausgabe Sieger und Ende der While-Schleife
			if (run.getFeldNummer(run.getFigurAmZug()) == DREIUNDSECHZIG) {
				System.out.println("Der Gewinner ist: " 
			+ run.getGewinnerFigur());
				running = false;
			} else {
				System.out.println("Spieler " 
			+ run.getFigurAmZug() 
				+ " ist am Zug\n" + "Du befindest dich auf Feld: "
						+ run.getFeldNummer(run.getFigurAmZug()));
				int wuerfel = UserInput.wuerfel();
				run.move(wuerfel);
			}
		} while (running);
	}
}

package de.ostfalia.prog.ss17;

/**
 * Stellt ein Sprungereignis dar.
 * 
 * @author Arne
 *
 */
public class SprungEreignis implements IEreignis {

	/**
	 * Nummer des Feldes, auf welches gesprungen werden soll.
	 */
	private int feldnummer;

	/**
	 * Erstellt ein Sprungereignis, welches die Feldnummer beinhaltet, auf
	 * welche gesprungen werden soll.
	 * 
	 * @param feldnummer
	 *            - Auf welches Feld gesprungen werden soll.
	 */
	public SprungEreignis(int feldnummer) {
		this.feldnummer = feldnummer;
	}

	/**
	 * @return the feldNummer
	 */
	public int getFeldnummer() {
		return feldnummer;
	}

	/**
	 * @return the ereignisTyp
	 */
	public EreignisTyp getEreignisTyp() {
		return EreignisTyp.SPRING_AUF_FELD;
	}

}

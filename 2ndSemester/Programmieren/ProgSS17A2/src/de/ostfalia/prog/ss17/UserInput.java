package de.ostfalia.prog.ss17;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Klasse für Spieler Eingabe.
 * @author Arne
 *
 */
public class UserInput {
	
/**
 * Methode zum Auswaehlen der Spieler.
 * @return - Farbenarray
 */
	public static Farbe[] anfangsFragen() {
		Scanner sc = new Scanner(System.in);
		List<String> farbwahl = new ArrayList<String>();
		farbwahl.add("BLAU");
		farbwahl.add("GELB");
		farbwahl.add("GRUEN");
		farbwahl.add("ROT");
		farbwahl.add("SCHWARZ");
		farbwahl.add("WEISS");

		Farbe[] farben;
		int inp;

		do {
			System.out.println("Wie viele Spieler gibt es? (2-6)");
			inp = sc.nextInt();
		} while (inp < 2 || inp > 6);
		farben = new Farbe[inp];

		// if (farben.length < 2 || farben.length > 6) {
		// // TODO: EXCEPTION WERFEN
		// }

		System.out.println("Bitte Farbe waehlen:");
		for (int i = 0; i < farben.length; i++) {
			System.out.println("Spieler " + (i + 1));
			for (int j = 0; j < farbwahl.size(); j++) {
				System.out.println(j + ". " + farbwahl.get(j));
			}
			inp = sc.nextInt();
			switch (farbwahl.get(inp)) {
			case "BLAU":
				farben[i] = Farbe.BLAU;
				farbwahl.remove("BLAU");
				break;
			case "GELB":
				farben[i] = Farbe.GELB;
				farbwahl.remove("GELB");
				break;
			case "GRUEN":
				farben[i] = Farbe.GRUEN;
				farbwahl.remove("GRUEN");
				break;
			case "ROT":
				farben[i] = Farbe.ROT;
				farbwahl.remove("ROT");
				break;
			case "SCHWARZ":
				farben[i] = Farbe.SCHWARZ;
				farbwahl.remove("SCHWARZ");
				break;
			case "WEISS":
				farben[i] = Farbe.WEISS;
				farbwahl.remove("WEISS");
				break;
			default:
				break;
			}
		}
		sc.close();
		return farben;
	}
	
	/**
	 * 
	 * @return -Gewuerfelte Augenanzahl
	 */
	public static int wuerfel() {
		System.out.println("Bitte würfeln...");
		int wuerfel = (int)(Math.random() * 6 + 1) +
				(int)(Math.random() * 6 + 1);
		System.out.println("Du hast gewuerfelt: " + wuerfel);
		return wuerfel;
	}
}

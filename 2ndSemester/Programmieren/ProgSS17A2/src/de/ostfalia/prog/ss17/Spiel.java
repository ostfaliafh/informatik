package de.ostfalia.prog.ss17;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author D. Thümen, A. Schernich
 *
 */
public class Spiel implements ISpiel {

	private List<Spieler> spielerListe = new LinkedList<Spieler>();
	private Spieler aktuellerSpieler;
	private GaensespielSpielfeld spielfeld;
	private boolean nocheinmalWuerfeln = false;

	/**
	 * Konstruktor initialisiert die Liste der Spieler bzw. Spielfiguren auf
	 * einem Standard Gänsespiel -Spielbrett (laut Aufgabebeschreibung). Die
	 * Reihenfolge der Farbe entspricht der Reihenfolge wie die Figuren gespielt
	 * werden. Die Figuren werden darüber hinaus standardmäßig auf Position 0
	 * des Spielfeldes platziert.
	 *
	 * @param farben
	 *            Array mit Farben.
	 */
	public Spiel(Farbe[] farben) {

		// TODO FalscheFigurenException

		// Erzeugung einer Spielerliste in korrekter Reihenfolge
		for (Farbe e : farben) {
			spielerListe.add(new Spieler(e));
		}

		aktuellerSpieler = spielerListe.get(0);

		this.spielfeld = new GaensespielSpielfeld();
		Feld[] felder = this.spielfeld.getFelder();

		for (Spieler s : spielerListe) {
			felder[0].setzeSpieler(s);
		}

	}

	/**
	 * Konstruktor initialisiert die Liste der Spieler bzw. Spielfiguren samt
	 * deren Anfangsposition. Die Reihenfolge der Farbe entspricht der
	 * Reihenfolge wie die Figuren gespielt werden. Darüber hinaus werden die
	 * Figuren auf die Position gebracht, die aus dem String hervorgeht
	 *
	 * @param strFarbePosition
	 *            Kommagetrennter String mit dem Format "Position:Farbe" (z.B.
	 *            "0:GELB, 10:ROT, 1:BLAU").
	 */
	public Spiel(String strFarbePosition) {
		// TODO FalscheFigurenException

		this.spielfeld = new GaensespielSpielfeld();
		Feld[] felder = this.spielfeld.getFelder();

		String[] part = strFarbePosition.split(", ");
		String[] part2;
		int feldNummer;
		String farbe;

		List<String> farbwahl = new ArrayList<String>();
		farbwahl.add("BLAU");
		farbwahl.add("GELB");
		farbwahl.add("GRUEN");
		farbwahl.add("ROT");
		farbwahl.add("SCHWARZ");
		farbwahl.add("WEISS");

		for (int i = 0; i < part.length; i++) {
			part2 = part[i].split(":");
			feldNummer = Integer.parseInt(part2[0]);
			farbe = part2[1];

			Spieler spieler = new Spieler(Farbe.valueOf(farbe));
			felder[feldNummer].setzeSpieler(spieler);
			farbwahl.remove(farbe);

			// if (farbe.equals("BLAU")) {
			// spieler = new Spieler(Farbe.BLAU);
			// felder[feldNummer].setzeSpieler(spieler);
			// farbwahl.remove("BLAU");
			// } else if (farbe.equals("GELB")) {
			// spieler = new Spieler(Farbe.GELB);
			// felder[feldNummer].setzeSpieler(spieler);
			// farbwahl.remove("GELB");
			// } else if (farbe.equals("GRUEN")) {
			// spieler = new Spieler(Farbe.GRUEN);
			// felder[feldNummer].setzeSpieler(spieler);
			// farbwahl.remove("GRUEN");
			// } else if (farbe.equals("ROT")) {
			// spieler = new Spieler(Farbe.ROT);
			// felder[feldNummer].setzeSpieler(spieler);
			// farbwahl.remove("ROT");
			// } else if (farbe.equals("SCWHARZ")) {
			// spieler = new Spieler(Farbe.SCHWARZ);
			// felder[feldNummer].setzeSpieler(spieler);
			// farbwahl.remove("SCHWARZ");
			// } else if (farbe.equals("WEISS")) {
			// spieler = new Spieler(Farbe.WEISS);
			// felder[feldNummer].setzeSpieler(spieler);
			// farbwahl.remove("WEISS");
			// }

			spielerListe.add(spieler);

			if (i == 0) {
				aktuellerSpieler = spieler;
			}
		}

	}

	/**
	 * Funktion:<br>
	 * Fuehrt Move aus:<br>
	 * Bewegt den Spieler und prueft, ob dieser auf Sonderfeld ist.<br>
	 * Parameter:<br>
	 * 
	 * @param augenzahl
	 *            - Uebergebenes Wuerfelergebnis
	 * 
	 */
	public void move(int augenzahl) {

		if (augenzahl < 2 || augenzahl > 12) {
			throw new UngueltigeAugenzahlException(
					"Es wurde " + augenzahl + " gewuerfelt. Es ist aber nur ein Wert zwischen 2 und 12 erlaubt.");
		}

		Feld aktuellesFeld = this.spielfeld.getFeldBeinhaltetSpieler(aktuellerSpieler);
		int neueFeldnummer = aktuellesFeld.getFeldnummer() + augenzahl;

		this.setzeSpielerAufFeld(aktuellesFeld, neueFeldnummer, augenzahl);

		// Falls "nocheinmalWuerfeln" durch "reagiereAufEreignis" auf true
		// gesetzt -> aktueller Spieler erneut aktueller Spieler.
		// Sonst neuer Spieler
		if (!nocheinmalWuerfeln) {
			this.setNaechsterSpieler();
		} else {
			this.nocheinmalWuerfeln = false;
		}
	}

	/**
	 * Ueberspringt Spieler die Aussetzen muessen.
	 */
	private void setNaechsterSpieler() {

		// Entfernen und hinten anfuegen
		spielerListe.remove(aktuellerSpieler);
		spielerListe.add(aktuellerSpieler);
		aktuellerSpieler = spielerListe.get(0);

		// Falls aktueller Spieler isAussetzen == true;
		while (aktuellerSpieler.isAussetzen()) {
			aktuellerSpieler.setAussetzen(false);
			spielerListe.remove(aktuellerSpieler);
			spielerListe.add(aktuellerSpieler);
			aktuellerSpieler = spielerListe.get(0);
		}
	}

	private void setzeSpielerAufFeld(Feld aktuellesFeld, int neueFeldnummer, int wuerfelergebnis) {

		Feld neuesFeld;
		/*
		 * Sonderfall, wenn das letzte Spielfeld uebersprungen werden wuerde.
		 */
		if (neueFeldnummer >= this.spielfeld.getFelder().length) {
			neueFeldnummer = spielfeld.getFelder().length - 1 - wuerfelergebnis;
		}

		// Entfernen des Spielers von seinem aktuellen Feld
		aktuellesFeld.entferneSpieler(aktuellerSpieler);

		neuesFeld = this.spielfeld.getFelder()[neueFeldnummer];

		// Direkte Abfrage ob neues Feld ein Ereignisfeld ist -> direkte
		// Ausfuehrung des Ereignisses
		if (neuesFeld instanceof IEreignisFeld) {
			IEreignis ereignis = ((IEreignisFeld) neuesFeld).setzeSpieler(aktuellerSpieler, wuerfelergebnis);
			this.reagiereAufEreignis(ereignis, wuerfelergebnis);
		} else {
			neuesFeld.setzeSpieler(aktuellerSpieler);
		}
	}

	private void setzeSpielerAufFeld(int neueFeldnummer, int wuerfelergebnis) {
		Feld aktuellesFeld = this.spielfeld.getFeldBeinhaltetSpieler(aktuellerSpieler);
		this.setzeSpielerAufFeld(aktuellesFeld, neueFeldnummer, wuerfelergebnis);
	}

	/**
	 * Getter, um Spieler am Zug zu liefern.
	 * 
	 * @return gibt farbe zurück
	 */
	public Farbe getFigurAmZug() {
		return aktuellerSpieler.getFarbe();
	}

	/**
	 * Getter, um die Spielfigur auf finalem Feld zu ermitteln.
	 * 
	 * @return - this.spieler (Gewinner)
	 */
	public Farbe getGewinnerFigur() {
		if (spielfeld.getFelder()[spielfeld.getFelder().length - 1].getSpielerList().isEmpty()) {
			return null;
		} else {
			return spielfeld.getFelder()[spielfeld.getFelder().length - 1].getSpielerList().get(0).getFarbe();
		}
	}

	/**
	 * Liefert die Nummer des Feldes, wo die gesuchte Figur sich befindet
	 * 
	 * @param farbe
	 *            Die gesuchte Figur, welche anhand ihrer Farbe repräsentiert
	 *            wird
	 * @return die Nummer des Feldes (0 ... 63), wo die gesuchte Figur sich
	 *         befindet, sonst -1, wenn die Figur sich nicht auf dem Spielfeld
	 *         befindet
	 */
	public int getFeldNummer(Farbe farbe) {
		try {
			return spielfeld.getFeldBeinhaltetFarbe(farbe).getFeldnummer();
		} catch (NullPointerException e) {
			return -1;
		}

	}

	/**
	 * todo:<br>
	 * Speichert Spielstand von .txt.
	 * 
	 * @param dateiName
	 *            - übergibt dateiName
	 * @throws IOException
	 *             - wirft fehler
	 */
	public void speichern(String dateiName) throws IOException {
		String spielstand = "";
		Farbe farbe;
		String dateiname = dateiName;

		// Feld[] felder = spielfeld.getFelder();

		// for (Feld f : felder) {
		// for (Spieler s : f.getSpielerList()) {
		// if (s != null) {
		// spielstand += f.getFeldnummer() + ":";
		// farbe = s.getFarbe();
		// switch (farbe) {
		// case BLAU:
		// spielstand += "BLAU" + ", ";
		// break;
		// case GELB:
		// spielstand += "GELB" + ", ";
		// break;
		// case GRUEN:
		// spielstand += "GRUEN" + ", ";
		// break;
		// case ROT:
		// spielstand += "ROT" + ", ";
		// break;
		// case SCHWARZ:
		// spielstand += "SCWHARZ" + ", ";
		// break;
		// case WEISS:
		// spielstand += "WEISS" + ", ";
		// break;
		// default:
		// break;
		//
		// }
		// }
		// }
		// }

		for (int i = 0; i < spielerListe.size(); i++) {
			int nr = spielfeld.getFeldBeinhaltetSpieler(aktuellerSpieler).getFeldnummer();
			spielstand += nr + ":" + aktuellerSpieler.getFarbe().name() + ", ";
			setNaechsterSpieler();
		}

		String tmp = "";
		for (int i = 0; i < spielstand.length() - 2; i++) {
			tmp += spielstand.charAt(i);
		}
		spielstand = tmp;

		System.out.println(spielstand);

		FileWriter fw = new FileWriter(dateiname);
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write(spielstand);

		bw.close();
		fw.close();

	}

	public static ISpiel ladenSpiel(String dateiName) throws IOException {
		FileReader line = null;
		String data = "";

		try {
			line = new FileReader(dateiName);
			BufferedReader br = new BufferedReader(line);
			while ((data = br.readLine()) != null) {
				System.out.println("data: " + data);
				Spiel neu = new Spiel(data);
				return neu;
			}

		} catch (FileNotFoundException e) {
			System.out.println("Datei nicht gefunden");
		} catch (NullPointerException e) {
			System.out.println("Fehlerhafte Datei");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * ToString
	 */
	@Override
	public String toString() {

		return spielfeld.getFeldBeinhaltetSpieler(aktuellerSpieler).getFeldnummer() + ":"
				+ aktuellerSpieler.getFarbe().name();
	}

	/**
	 * Reagieren auf Ereignisfeld
	 * 
	 * @param ereignis
	 *            - spezifisches ereignis
	 * @param wuerfelergebnis
	 *            - Uebergebene Wuerfelzahl
	 */
	private void reagiereAufEreignis(IEreignis ereignis, int wuerfelergebnis) {

		if (ereignis instanceof AussetzenEreignis) {
			this.aktuellerSpieler.setAussetzen(true);
		} else if (ereignis instanceof SprungEreignis) {
			SprungEreignis sprungEreignis = (SprungEreignis) ereignis;
			this.setzeSpielerAufFeld(sprungEreignis.getFeldnummer(), wuerfelergebnis);
		} else if (ereignis instanceof WuerfelEreignis) {
			this.nocheinmalWuerfeln = true;
		} // Else nothing happens
	}
}
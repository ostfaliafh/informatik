package de.ostfalia.prog.ss17;

public class AussetzenFeld extends Feld implements IEreignisFeld {

	public AussetzenFeld(int feldnummer) {
		super(feldnummer);
	}

	@Override
	public IEreignis setzeSpieler(Spieler spieler, int augenzahl) {
		super.setzeSpieler(spieler);
		return new AussetzenEreignis();
	}
	
}

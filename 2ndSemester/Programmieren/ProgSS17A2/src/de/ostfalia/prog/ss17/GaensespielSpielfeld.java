package de.ostfalia.prog.ss17;

public class GaensespielSpielfeld {

	private Feld[] felder;

	/**
	 * Konstruktor zum Erstellen eines Spielfeldes.
	 */
	public GaensespielSpielfeld() {
		felder = new Feld[64];

		for (int i = 0; i < felder.length; i++) {

			switch (i) {
			case 6:
				felder[i] = new SprungFeld(i, 12);
				break;
			case 5:
			case 9:
				felder[i] = new SprungUmAugenzahlFeld(i, false);
				break;
			case 14:
			case 18:
			case 27:
			case 32:
			case 36:
			case 50:
			case 59:
				felder[i] = new SprungUmAugenzahlFeld(i, true);
				break;
			case 19:
				felder[i] = new AussetzenFeld(i);
				break;
			case 23:
				felder[i] = new ZurueckOderAussetzenFeld(i, 14, 18);
				break;
			case 31:
				felder[i] = new WuerfelFeld(i);
				break;
			case 41:
				felder[i] = new ZurueckOderAussetzenFeld(i, 32, 36);
				break;
			case 42:
				felder[i] = new SprungFeld(i, 30);
				break;
			case 45:
				felder[i] = new ZurueckOderAussetzenFeld(i, 36);
				break;
			case 52:
				felder[i] = new WuerfelFeld(i, 12);
				break;
			case 54:
				felder[i] = new ZurueckOderAussetzenFeld(i, 50);
				break;
			case 58:
				felder[i] = new SprungFeld(i, 0);
				break;
			default:
				felder[i] = new Feld(i);
				break;
			}
		}
	}

	public Feld[] getFelder() {
		return this.felder;
	}

	public Feld getFeldBeinhaltetSpieler(Spieler spieler) {
		Feld beinhaltetSpieler = null;
		for (Feld f : felder) {
			if (f.getSpielerList().contains(spieler)) {
				return f;
			}
		}
		return beinhaltetSpieler;
	}

	public Feld getFeldBeinhaltetFarbe(Farbe farbe) {
		Feld beinhaltetFarbe = null;
		for (Feld feld : felder) {
			for (Spieler s : feld.getSpielerList()) {
				Farbe f = s.getFarbe();
				if (farbe.equals(f)) {
					return feld;
				}
			}
		}
		return beinhaltetFarbe;
	}
}

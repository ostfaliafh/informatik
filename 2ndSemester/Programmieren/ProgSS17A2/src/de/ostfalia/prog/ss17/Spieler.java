package de.ostfalia.prog.ss17;

public class Spieler {

	private boolean aussetzen;
	private Farbe farbe;

	/**
	 * Konstruktor für Spielbeginn auf Feld 0
	 * 
	 * @param farbe
	 */
	public Spieler(Farbe farbe) {
		this.setFarbe(farbe);
		this.setAussetzen(false);
	}

	/**
	 * Konstruktor für z.B. Laden
	 * 
	 * @param farbe
	 */
	public Spieler(Farbe farbe, boolean aussetzen) {
		this.setFarbe(farbe);
		this.setAussetzen(aussetzen);
	}

	/**
	 * @return the aussetzen
	 */
	public boolean isAussetzen() {
		return aussetzen;
	}

	/**
	 * @param aussetzen the aussetzen to set
	 */
	public void setAussetzen(boolean aussetzen) {
		this.aussetzen = aussetzen;
	}

	/**
	 * @return the farbe
	 */
	public Farbe getFarbe() {
		return farbe;
	}

	/**
	 * @param farbe the farbe to set
	 */
	public void setFarbe(Farbe farbe) {
		this.farbe = farbe;
	}
}
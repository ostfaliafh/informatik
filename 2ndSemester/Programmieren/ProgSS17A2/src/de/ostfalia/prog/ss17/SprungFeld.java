package de.ostfalia.prog.ss17;

/**
 * Beschreibt eine Unterklasse von Feld. Wenn eine Spielfigur auf dieses Feld
 * kommt, wird sie auf ein anderes Feld verschoben.
 * 
 * @author Arne
 *
 */
public class SprungFeld extends Feld implements IEreignisFeld {

	int sprungFeld;

	/**
	 * Ruft den Konstruktor von Feld auf und setzt das Sprungfeld auf das
	 * angegebene Feld.
	 * 
	 * @param feldnummer
	 *            - Feldnummer die dieses Feld widerspiegelt.
	 * @param sprungFeld
	 *            - Eine Figur, die auf dieses Feld gesetzt wird, wird weiter
	 *            auf das angegebene Feld geschoben.
	 */
	public SprungFeld(int feldnummer, int sprungFeld) {
		super(feldnummer);
		this.sprungFeld = sprungFeld;
	}

	@Override
	public IEreignis setzeSpieler(Spieler spieler, int augenzahl) {
		super.setzeSpieler(spieler);
		return new SprungEreignis(this.sprungFeld);
	}

}

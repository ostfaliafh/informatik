package de.ostfalia.prog.ss17;

public class AussetzenEreignis implements IEreignis {

	@Override
	public EreignisTyp getEreignisTyp() {
		return EreignisTyp.AUSSETZEN;
	}

}

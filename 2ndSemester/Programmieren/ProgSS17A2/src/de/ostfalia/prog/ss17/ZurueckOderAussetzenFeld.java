package de.ostfalia.prog.ss17;

public class ZurueckOderAussetzenFeld extends Feld implements IEreignisFeld {

	private int[] aussetzenFelder;

	public ZurueckOderAussetzenFeld(int feldnummer, int... aussetzenFelder) {
		super(feldnummer);
		this.setAussetzenFelder(aussetzenFelder);
	}

	/**
	 * @return the aussetzenFelder
	 */
	public int[] getAussetzenFelder() {
		return aussetzenFelder;
	}

	/**
	 * @param aussetzenFelder
	 *            the aussetzenFelder to set
	 */
	public void setAussetzenFelder(int[] aussetzenFelder) {
		this.aussetzenFelder = aussetzenFelder;
	}

	@Override
	public IEreignis setzeSpieler(Spieler spieler, int augenzahl) {
		super.setzeSpieler(spieler);
		int vorherigesFeld = this.getFeldnummer() - augenzahl;
		/*
		 *  Ueberprueft, ob Spieler von einem Feld kommt, welches dazu fuehrt,
		 *  dass er die naechste Runde aussetzen muss.
		 */
		boolean vonAussetzenFeld = false;
		
		for (int feld : this.aussetzenFelder) {
			if (feld == vorherigesFeld) {
				vonAussetzenFeld = true;
			}
		}
		
		if (vonAussetzenFeld) {
			return new AussetzenEreignis();
		} else {
			return new SprungEreignis(vorherigesFeld);
		}
	}
}

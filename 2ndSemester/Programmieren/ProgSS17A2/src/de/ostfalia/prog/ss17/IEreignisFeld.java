package de.ostfalia.prog.ss17;

public interface IEreignisFeld {
	IEreignis setzeSpieler(Spieler spieler, int augenzahl);
}

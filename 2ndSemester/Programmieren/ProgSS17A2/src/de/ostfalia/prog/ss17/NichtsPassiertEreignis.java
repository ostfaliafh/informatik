package de.ostfalia.prog.ss17;
/**
 * besonderes Ereignis
 * @author A. Schernich, D. Thümen
 *
 */
public class NichtsPassiertEreignis implements IEreignis {

	@Override
	public EreignisTyp getEreignisTyp() {
		return EreignisTyp.NICHTS_PASSIERT;
	}

}

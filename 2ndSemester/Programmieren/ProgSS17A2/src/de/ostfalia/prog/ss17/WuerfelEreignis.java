package de.ostfalia.prog.ss17;

/**
 * Ein spezielles Ereignis mit bestimmten Rueckggabwert.
 * @author Arne
 *
 */
public class WuerfelEreignis implements IEreignis {

	@Override
	public EreignisTyp getEreignisTyp() {
		return EreignisTyp.WUERFEL_NOCHMAL;
	}
}
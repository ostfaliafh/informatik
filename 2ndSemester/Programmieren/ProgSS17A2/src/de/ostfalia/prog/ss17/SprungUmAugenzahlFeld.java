package de.ostfalia.prog.ss17;

/**
 * Stellt ein Feld dar, bei welchem die Spielfigur um die gewuerfelte Augenzahl
 * nach vorne oder zurück zieht.
 * 
 * @author Arne
 *
 */
public class SprungUmAugenzahlFeld extends Feld implements IEreignisFeld {

	private boolean vorwaerts;

	public SprungUmAugenzahlFeld(int feldnummer, boolean vorwaerts) {
		super(feldnummer);
		this.setVorwaerts(vorwaerts);
	}

	/**
	 * Die Spielfigur wird als default Wert nach vorne gezogen.
	 * @param feldnummer
	 */
	public SprungUmAugenzahlFeld(int feldnummer) {
		this(feldnummer, true);
	}

	/**
	 * @return the vorwaerts
	 */
	public boolean isVorwaerts() {
		return vorwaerts;
	}

	/**
	 * @param vorwaerts the vorwaerts to set
	 */
	public void setVorwaerts(boolean vorwaerts) {
		this.vorwaerts = vorwaerts;
	}

	@Override
	public IEreignis setzeSpieler(Spieler spieler, int augenzahl) {
		super.setzeSpieler(spieler);
		int neuesFeld = this.getFeldnummer();
		neuesFeld += vorwaerts ? augenzahl : -augenzahl;
		return new SprungEreignis(neuesFeld);
	}

}

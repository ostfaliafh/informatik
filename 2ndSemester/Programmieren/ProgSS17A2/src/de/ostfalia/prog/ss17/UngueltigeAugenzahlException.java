package de.ostfalia.prog.ss17;

public class UngueltigeAugenzahlException extends RuntimeException {

	public UngueltigeAugenzahlException(String string) {
		super(string);
	}

	/**
	 * Default serial version uid.
	 */
	private static final long serialVersionUID = 1L;
}
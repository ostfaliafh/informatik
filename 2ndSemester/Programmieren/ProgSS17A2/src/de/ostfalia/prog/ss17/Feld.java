package de.ostfalia.prog.ss17;

import java.util.LinkedList;
import java.util.List;

/**
 * Klasse für ein Standartfeld.
 * @author - A. Schernich, D. Thümen
 *
 */
public class Feld {

	private int feldnummer;
	private LinkedList<Spieler> spielerAufFeld;

	/**
	 * Konstruktor für alle einfachen Felder.
	 * 
	 * @param feldnummer
	 *            - Die Feldnummer.
	 */
	public Feld(int feldnummer) {
		this.setFeldnummer(feldnummer);
		spielerAufFeld = new LinkedList<Spieler>();
	}

	/**
	 * Setter der Feldnummer.
	 * 
	 * @param feldnummer - aktuelle Feldnummer.
	 */
	public void setFeldnummer(int feldnummer) {
		this.feldnummer = feldnummer;
	}

	/**
	 * TODO Einfach kommentieren was passiert, also wenn spieler schon
	 * vorhanden, dann nicht erneut hinzugefügt
	 * 
	 * @param spieler - aktueller Spieler
	 */
	public void setzeSpieler(Spieler spieler) {
		if (!spielerAufFeld.contains(spieler)) {
			spielerAufFeld.add(spieler);
		}
	}
/**
 * Methode entfernt einen Spieler von Feld.
 * @param spieler - aktueller Spieler
 */
	public void entferneSpieler(Spieler spieler) {
		spielerAufFeld.remove(spieler);
	}

	/**
	 * Getter für die Feldnummer
	 * 
	 * @return feldNummer
	 */
	public int getFeldnummer() {
		return this.feldnummer;
	}

	/**
	 * Getter für die Spielerliste.
	 * @return - Rueckgabe der auf dem Feld befindenden
	 * Spieler in Form einer Liste.
	 */
	public List<Spieler> getSpielerList() {
		return this.spielerAufFeld;
	}

}

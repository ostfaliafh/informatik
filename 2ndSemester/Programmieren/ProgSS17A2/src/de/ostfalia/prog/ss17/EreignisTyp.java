package de.ostfalia.prog.ss17;

/**
 * Enthaelt die zu erwartenden Ereignistypen.
 * 
 * @author Arne
 *
 */
public enum EreignisTyp {
	WUERFEL_NOCHMAL, SPRING_AUF_FELD, AUSSETZEN, NICHTS_PASSIERT;
}
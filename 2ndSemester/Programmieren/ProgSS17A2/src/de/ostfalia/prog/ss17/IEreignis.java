package de.ostfalia.prog.ss17;
/**
 * Interface zur Strukturierung von Ereignissen.
 * @author - A. Schernich, D. Thümen
 *
 */
public interface IEreignis {
	/**
	 * 
	 * @return - Gibt Typ aus enum Klasse zurueck
	 */
	EreignisTyp getEreignisTyp();
}
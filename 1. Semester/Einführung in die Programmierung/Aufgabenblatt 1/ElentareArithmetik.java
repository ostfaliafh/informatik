package de.ostfalia.gdp.ws16.s1;
import java.util.InputMismatchException;


/**
 * This is a small Project with some Math stuff
 * Author Daniel Thümen
 */
import java.util.Scanner;
public class ElentareArithmetik {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = 0;
		int b = 0;
		boolean rechner = true;
		
		while(rechner == true) {
			try {
				System.out.println("Bitte geben Sie Ihre wunsch Zahlen ein, welche Berechnet werden sollen...\n"
						+ "Zahl 1:");
				a = sc.nextInt();
				System.out.println("Zahl 2:");
				b = sc.nextInt();
				rechner = false;
			} catch (InputMismatchException aex){
				System.out.println("Bitte geben Sie richtige Zahlen ein\n\n\n");
				sc.next();
			}
		}
		if (rechner == false) {
			addition(a, b);
			subtraktion(a, b);
			multiplikation(a, b);
			division(a, b);
			divqua(a, b);
		}

		
		sc.close();
	}
	public static void addition(int a, int b) {
		int erg = a + b;
		System.out.println(a + " + " + b  + " = " + erg);
	}
	public static void subtraktion(int a, int b) {
		int erg = a - b;
		System.out.println(a + " - " + b + " = " + erg);
	}
	public static void multiplikation(int a, int b) {
		int erg = a * b;
		System.out.println(a + " * " + b + " = " + erg);
	}
	public static void division(int a, int b) {
		try {
			int erg = a / b;
			System.out.println(a + " / " + b + " = " + erg);
		} catch(ArithmeticException ae) {
			System.out.println("Du kannst nicht durch 0 dividieren");
		}
	}
	public static void divqua(int a, int b) {
		int erg = a * a - b * b;
		System.out.println(a + " * " + a + " - " + b + " * " + b + " = " + erg);
	}
	
}

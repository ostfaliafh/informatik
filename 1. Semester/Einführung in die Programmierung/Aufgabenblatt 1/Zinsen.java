package de.ostfalia.gdp.ws16.s1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zinsen {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int time = 0;
		double zins = 0, kap = 0, zinsen = 0, endzins = 0;
		boolean math = true;
		
		while (math == true) {
			try {
				System.out.println("Daten für Zins berechnung eingeben\n");
				System.out.print("Kapital: ");
				kap = sc.nextDouble();
				System.out.print("Zinssatz: ");
				zins = sc.nextDouble();
				System.out.print("Zeitraum (In Jahre): ");
				time = sc.nextInt();
				math = false;
			} catch (InputMismatchException ae){
				System.out.println("Bitte geben Sie richtige Zahlen ein");
				sc.next();
			}
		}
		if (math == false) {
			for (int i = 0; i < time; i++) {
				zinsen = kap * zins / 100;
				kap = kap + Math.floor(zinsen);
				endzins = endzins + Math.floor(zinsen);
			}
			System.out.println("Nach " + time + " Jahren beträgt Ihr Konto folgende Daten:\n"
					+ "Kontostand: "+ kap + "\n"
					+ "Verzinsung: " + endzins);
		}
		sc.close();
	}

}

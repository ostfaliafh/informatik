package de.ostfalia.gdp.ws16.s1;

import java.util.Scanner;

public class Zeitberechnung {
	public static void main (String[] args) {
		int Monate; //Monate
		int d; //Tage
		System.out.println("Anzahl der Tage:");
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		d = a % 30;
		Monate = a/30;
		System.out.println("Es sind " + Monate + " Monate und " + d + " Tage");
		sc.close();
	}

}

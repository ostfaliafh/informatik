package de.ostfalia.gdp.ws16.s1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Dreiecksberechnung {
	public static void main (String[] args) {
		int ax = 0, ay = 0, bx = 0, by = 0, cx = 0, cy = 0;
		Scanner sc = new Scanner(System.in);
		boolean math = true;
	
		while(math == true) {
			try {
				System.out.println("Bitte geben Sie nacheinander die Punkte Ihres Dreieckes an\n"
						+ "Punkt A:");
				System.out.print("X: ");
				ax = sc.nextInt();
				System.out.print("Y: ");
				ay = sc.nextInt();
				System.out.println("Punkt B:");
				System.out.print("X: ");
				bx = sc.nextInt();
				System.out.print("Y: ");
				by = sc.nextInt();
				System.out.println("Punkt C:");
				System.out.print("X: ");
				cx = sc.nextInt();
				System.out.print("Y: ");
				cy = sc.nextInt();
				math = false;
			} catch (InputMismatchException aex){
				System.out.println("Bitte geben Sie richtige Zahlen ein");
				sc.next();
			}
		}
		
		if (math == false) {
			berechnung(ax, ay, bx, by, cx, cy);
		}
		sc.close();
	}
		

	private static void berechnung(int ax, int ay, int bx, int by, int cx, int cy) {
		/**
		 * Vektoren von Ausgangspunkt A
		 */
		double ab1, ab2, ac1, ac2;
		
		/**
		 * Determinante
		 */
		double det;
		
		
		/**
		 * Berechnung der beiden Vektoren
		 */
		ab1 = bx - ax;
		//System.out.println(ab1);
		ab2 = by - ay;
		//System.out.println(ab2);
		ac1 = cx - ax;
		//System.out.println(ac1);
		ac2 = cy - ay;
		//System.out.println(ac2);
		/**
		 * Berechnung des Betrages der Determinate um den Flächeninhalt zu bekommen
		 */
		
		if (bx >= cx) {
			det = 0.5 * (ab1 * ac2 - ab2 * ac1);
			
			System.out.println("Die Fläche beträgt: " + det);
			
		} else {
			det = 0.5 * (ac1 * ab2 - ac2 * ab1);
			
			System.out.println("Die Fläche beträgt: " + det);
			
		}

	}
	


}

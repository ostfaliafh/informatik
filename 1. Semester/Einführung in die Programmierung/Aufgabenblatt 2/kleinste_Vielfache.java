package de.ostfalia.gdp.ws15.s2;

import java.util.Scanner;

public class kleinste_Vielfache {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int x, y, z = 0, ze;
		System.out.print("X: ");
		x = sc.nextInt();
		System.out.print("Y: ");
		y = sc.nextInt();
		
		if (x <= y) {
			if (y % x == 0) {
				z = y;
			} else {
				ze = (y / x) + 1;
				z = x * ze;
				//System.out.println(ze);
				
			}
			
		} else if (x > y) {
			ze = (x / y) +1;
			z = y * ze;
		}
		
		System.out.println("Das kleinste Vielfache von " + x +
				", welches mindestens so groß ist wie " + y + ", ist: " + z);
		sc.close();
	}
}

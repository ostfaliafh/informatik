package de.ostfalia.gdp.ws16.s4;

import java.util.Scanner;

public class ApproximatePi {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int input;
		double nen = 1, multi = 4, pi = 0, vergleich;
		input = sc.nextInt();
		
		for (int i = 1; i <= input; i++ ) {
			if (i % 2 != 0) {
				pi = pi + (1 / nen);
				//System.out.println("+" + nen);
			} else if (i % 2 == 0) {
				pi = pi - (1 / nen);
				//System.out.println("-" + nen);
			}
			nen = nen + 2;
		}
		multi = multi * pi;
		vergleich = ((Math.PI - multi) / Math.PI ) * (-1) * 100;
		
		System.out.println("Approximation: " + Math.round(multi * 10000.0) / 10000.0);
		System.out.println("Abweichung: " + Math.round(vergleich * 10000.0) / 10000.0);
		sc.close();
		}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s8;

import java.util.Arrays;

/**
 *
 * @author arch
 */
public class BitSet {
    public static void main (String[] args) {
        long bs = 0b0110100;
        int b = 3;
        System.out.println("Kardinalität: " + getCard(bs) + "\n"
                + "Kleinste Bit: " + getLowestSetBit(bs) + "\n"
                + "Größter Bit: " + getHighestSetBit(bs) + "\n"
                + "Inclube b: " + Long.toBinaryString(include(bs, b)) + "\n"
                + "isValid: " + isValid(bs) + "\n"
                + "Komplement: " + Long.toBinaryString(complement(bs)) + "\n"
                + "IsComplete: " + isComplete(bs) + "\n\n");
        
        printBits(bs);
        printNums(bs);
        System.out.println("Ab jetzt sogar mit Array...\n\n\n");
        
        long[] subsets = {0b0101101000, 0b010110110100, 0b100110100100}; 
        int m = 5;
        
        System.out.println("Union: " + Long.toBinaryString(union(subsets)) + "\n"
                + "Intersect: " + Long.toBinaryString(intersect(subsets)) + "\n"
                + "Komplement: " + Long.toBinaryString(complement(subsets)) + "\n"
                + "Meiste Teilmengen: " + Arrays.toString(getLargest(subsets)) + "\n"
                + "Unter der angegebenen Menge: " + Arrays.toString(getBelowLimit(subsets, m)) + "\n"
                + "above limit: " + Arrays.toString(getAboveLimit(subsets, m)) + "\n"
                + "frequenzies: " + Arrays.toString(getFrequencies(subsets)) + "\n"
                + "supersets: " + Arrays.toString(supersets(subsets, bs)) + "\n");
        printBits(subsets);
        printNums(subsets);
        
    }
    
    public static int getCard(long bs) {
        int z, anz = 0;
        if (bs > 0) {
            while (bs != 0) {
                z = (int)bs % 2;
                if (z == 1) {
                    anz++;
                }
                bs /= 2;
            }
            return anz;
        } else {
            return 0;
        }
    }
    
    public static int getLowestSetBit(long bs) {
        if (bs > 0) {
            if (bs % 2 != 0) {
                return 0;
            } else  {
                int z = 0;
                while (bs % 2 == 0) {
                    bs = bs >> 1;
                    z++;
                }
            return z;
            }
        } else {
            return 0;
        }
        
    }
    
    public static int getHighestSetBit(long bs) {
        int h = 0;
        for (int i = 1; i <= 50; i++) {
            if ((bs >>> i) % 2 != 0) {
                h = i;
            }
        }
        
        return h;
    }
    
    public static long include(long bs,int b) {
        bs = bs | (1<<b);
        return bs;
    }
    
    public static boolean isValid(long bs) {
        return bs % 2 != 0;
    }
    
    public static long complement(long bs) {
        return ((~bs >> 1) << 1) | (1 & bs);
    }
    
    public static boolean isComplete(long bs) {
        boolean check = true;
        for (int i = 1; i <= 50; i++) {
            if ((bs >>> i) % 2 == 0) {
                check = false;
            } 
        }
        return check;
    }
    
    public static void printBits(long bs) {
        System.out.println("Binärdarstellung: " + Long.toBinaryString(bs));
        
    }
    
    public static void printNums(long bs) {
        System.out.print("Zahlen aus der Menge: ");
        for (int i = 1; i <= 50; i++) {
            if ((bs >>> i) % 2 != 0) {
                System.out.print(i +", ");
            } 
        }
    }
    
    public static long union(long[] subsets) {
        long a = 0;
        for (int i = 0; i < subsets.length; i++) {
            a |= subsets[i] & ~1;
        }
        return a;
    }
    
    public static long intersect(long[] subsets) {
        if (subsets.length == 0) {
            return 0;
        }
        
        long a = subsets[0];
        
        for (int i = 0; i < subsets.length; i++) {
            a &= subsets[i] & ~1;
        }
        return a;
    }
    
    public static long complement(long[] subsets) {
        return complement(intersect(subsets));
    }
    
    public static long[] getLargest(long[] subsets) {
        int counter = 0, maxval = 0;
        for (int i = 0; i < subsets.length; i++) {
            int k = getCard(subsets[i]); 
            if (k > maxval) {
                counter = 1;
                maxval = k;
            } else if (k == maxval) {
                counter++;
            }
            
        }
        long[] ret = new long[counter];
        counter = 0;
        
        for (int i = 0; i < subsets.length; i++) {
            if (getCard(subsets[i]) == maxval) {
                ret[counter] = subsets[i];
                counter++;
            }
        }
        
        return ret;
    }
    
    public static long[] getBelowLimit(long[] subsets, int m) {
        
        int counter = 0;
        for (int i = 0; i < subsets.length; i++) {
            if (getHighestSetBit(subsets[i]) <= m) {
                counter++;
            }
        }
        if (counter == 0) {
            return new long[]{1};
        }
        
        long[] ret = new long[counter];
        counter = 0;
        for (int i = 0; i < subsets.length; i++) {
            if (getHighestSetBit(subsets[i]) <= m) {
                ret[counter] = subsets[i];
                counter++;
            }
        }
        
        return ret;
    }
    
    public static long[] getAboveLimit(long[] subsets, int m) {
        int counter = 0;
        for (int i = 0; i < subsets.length; i++) {
            if (getLowestSetBit(subsets[i]) >= m) {
                counter++;
            }
        }
        if (counter == 0) {
            return new long[]{1};
        }
        
        long[] ret = new long[counter];
        counter = 0;
        for (int i = 0; i < subsets.length; i++) {
            if (getLowestSetBit(subsets[i]) >= m) {
                ret[counter] = subsets[i];
                counter++;
            }
        }
        
        return ret;
    }
    
    public static int[] getFrequencies(long[] subsets) {
        int[] ret = new int[50];
        for (int i = 0 ; i < subsets.length; i++) {
            for (int j = 1; j < 50; j++) {
                if ((subsets[i] >>> j) % 2 == 1) {
                    ret[j] += 1;
                }
            }
        }
        
        return ret;
    }
    
    public static long[] supersets(long[] subsets, long bs) {
        int counter = 0;
        for (int i = 0; i < subsets.length; i++) {
            if ((subsets[i] | bs) == subsets[i]) {
                counter++;
            }
        }
        if (counter == 0) {
            return new long[] {0};
        }
        long[] ret = new long[counter];
        counter = 0;
        for (int i = 0; i < subsets.length; i++) {
            if ((subsets[i] | bs) == subsets[i]) {
                ret[counter] = subsets[i];
                counter++;
            }
        }
        return ret;
    }
    
    public static void printBits(long[] subsets) {
        for (int i = 0; i < subsets.length; i++) {
            printBits(subsets[i]);
        }
        
    }
    
    public static void printNums(long[] subsets) {
        for (int i = 0; i < subsets.length; i++) {
            printNums(subsets[i]);
        }
    }






    
}

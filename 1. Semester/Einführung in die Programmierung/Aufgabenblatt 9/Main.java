/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s9;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author arch
 */
public class Main {
    public static void main (String[] args) throws IOException {
        /**
         * Ware Einlesen
         */
        String shop = "";
        FileReader inp = null;
        try {
            inp = new FileReader("shop.csv");
        } catch (FileNotFoundException e){
            System.out.println("Datei nicht gefunden");
        }
        BufferedReader br = new BufferedReader(inp);
        while ((shop = br.readLine()) != null ) {
            OnlineShop.fuelleLager(shop);
        }
        //for (int i = 0; i < OnlineShop.db.length; i++) {
        //    System.out.println(OnlineShop.db[i]);
        //}
        
        /**
         * Anzeigen Menü
         */
        boolean check = true;
        int wahl;
        Scanner sc = new Scanner(System.in);
        
        do {
            System.out.println("Herzlich Willkommen im Shop XYZ\n"
                + "Bitte wählen Sie eine der folgenden Aktionen aus:\n"
                + "(1) Ware Bestellen\n"
                + "(2) Warenbestand Anzeigen\n"
                + "(3) Beenden");
            wahl = sc.nextInt();
            switch (wahl) {
                case 1: 
                    Scanner sc1 = new Scanner(System.in);
                    System.out.println();
                    String name;
                    int anz;
                    System.out.println("Name der Ware:");
                    name = sc1.nextLine();
                    System.out.println("Anzahl:");
                    anz = sc.nextInt();
                    System.out.println(OnlineShop.bestellen(name, anz));
                    break;
                case 2:
                    System.out.println();
                    OnlineShop.bestandsausgabe();
                    break;
                case 3:
                    check = false;
                    break;
                default:
                    System.out.println();
            }
        } while (check == true);
        
        
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s9;

import java.io.IOException;

public class OnlineShop {
    static int count = 0;
    static Ware[] db = new Ware[100];
    /**
     * Läd den Warenbestand aus einer csv-Datei.
     * csv-Zeile: Warenname, Preis, Bestand
     * Beispiel : Ball, 4.99, 25
     * @param shop - Namen der csv-Datei.
     * @throws IOException
     */
    public static void fuelleLager(String shop) throws IOException {
        String[] list = shop.split(", ");
        //System.out.println(list[0]);
        //Ware test = new Ware(list[0], Double.parseDouble(list[1]), Integer.parseInt(list[2]));
        Ware test = new Ware(list[0], Double.parseDouble(list[1]), Integer.parseInt(list[2]));
        db[count] = test;
        count++;
        
    }
    
    
    /**
     * Bestellung der Ware "warenName" in der gewünschte Menge "anzahl".
     * @param warenName - Name der Ware.
     * @param anzahl - gewünschte Menge
     * @return Preis für die gelieferte Warenmenge.
     */
    public static double bestellen(String warenName, int anzahl) {
        double price = 0;
        
        for (int i = 0; i < count; i++){
            if (warenName.equals(db[i].getName())) {
                if (anzahl <= db[i].getNumber()) {
                    price = anzahl * db[i].getPrice();
                    db[i].setNumber(db[i].getNumber() - anzahl);
                } else {
                    price = db[i].getNumber() * db[i].getPrice();
                    db[i].setNumber(0);
                }
            }
        }
        return price;
    }
    
    /**
     * Ausgabe des vorhandenen Lagerbestands auf der Konsole
     * Beispiel:
     * 0: Artikel: Ball, Preis: 4.99, Bestand: 25
     * 1: Artikel: Seil, Preis: 10.99, Bestand: 102 
     */
    public static void bestandsausgabe() {
        for (int i = 0; i < count; i++) {
            System.out.println(db[i]);
        }
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s9;

/**
 *
 * @author arch
 */
public class Wombat {
    private String art;
    private String colour;
    private int klaugen;
    private int graugen;
    private int position;
    
    public Wombat (String art, String colour, int klaugen, int graugen, int position) {
        this.art = art;
        this.colour = colour;
        this.klaugen = klaugen;
        this.graugen = graugen;
        this.position = position;
    }
    
    public String getArt () {
        return art;
    }
    
    public String getColour () {
        return colour;
    }
    
    public int getKlaugen () {
        return klaugen;
    }
    
    public int getGraugen() {
        return graugen;
    }
    
    public int getPosition() {
        return position;
    }
    
    public void setArt(String art) {
        this.art = art;
    }
    
    public void setColour(String colour) {
        this.colour = colour;
    }
    
    public void setKlaugen(int klaugen) {
        this.klaugen = klaugen;
    }
    
    public void setGraugen(int graugen) {
        this.graugen = graugen;
    }
    
    public void setPosition (int position) {
        this.position = position;
    }
    
    /**
     * Führt einen Zug für ein Wombat aus,
     * d.h. die Position des Wombats wird entsprechend der Regeln verändert.
     * @param w
     * @return zielErreicht - true, falls dieses Wombat die Ziellinie überschritten hat,
     *                         false, sonst
     */
    public boolean zug(int w){
        if (w <= klaugen) {
            this.position += w;
        } else {
            this.position += 1;
        }
        
        if(this.position % 10 == 0 && this.position > 0) {
            this.position += 3;
        }
        
        return this.position >= WombatRallye.spielfeld;
    }
    
    @Override
    public String toString() {
        return "" + this.position;
    }
    
    
    
}

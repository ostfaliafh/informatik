/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s9;

import java.util.Scanner;

/**
 *
 * @author arch
 */
public class WombatRallye {
    static Spieler[] players;
    static int spielfeld;
    
    
    public static void main (String[] args) {
        
        Scanner sc = new Scanner(System.in);
        int fields, playerno, simu, counter = 0;
        System.out.println("Wie viele Spielfelder gibt es?");
        fields = sc.nextInt();
        System.out.println("Wie viele Spieler spielen mit? (3-6)");
        playerno = sc.nextInt();
        players = new Spieler[playerno];
        System.out.println("Wie viele Runden sollen Simuliert werden?");
        simu = sc.nextInt();
        String colour = "";
        Scanner sc1 = new Scanner(System.in);
        for (int i = 0; i < playerno; i++ ) {
            System.out.print("Farbe: ");
            colour = sc1.nextLine();
            Wombat tmp_wombat = new Wombat ("emsig", colour, 3, 4, 0);
            Spieler tmp_spieler = new Spieler(colour, tmp_wombat);
            players[i] = tmp_spieler;
        }
        
        for (int i = 0; i < simu; i++) {
            init(fields, playerno);
            while (simulateRunde() == null) {
                counter++;
            }
        }
        
        System.out.println("Die Wombat-Rallye war " + spielfeld + " Spielfelder lang.\n"
                + "Es waren " + (players.length) + " Wombats am Start.\n"
                + "Durchschnittlich wurden " + ((double)counter / simu) + " Runden gespielt, bis zum Sieg.");
        
    }
    /**
     * Erzeugt ein Spielfeld mit der gewünschten Anzahl von Feldern
     * erzeugt für jeden Spieler ein emsiges, kleines Wombat
     * und positioniert alle Wombats auf dem Feld 0
     * @param anzahlFelder - Anzahl der Felder des Spielfelds
     * @param anzahlSpieler - Anzahl der Mitspieler
     */
    public static void init(int anzahlFelder, int anzahlSpieler) {
        spielfeld = anzahlFelder;
        for (int i = 0; i < anzahlSpieler; i++) {
            players[i].getWombat().setPosition(0);
            
        }
        
    }
    
    /**
     * Simuliert eine Runde in der Wombat Ralley,
     * d.h. jedes Wombat zieht einmal.
     * @return sieger - null, solange noch kein Sieger feststeht
     */
    public static Wombat simulateRunde(){
        Wombat fin = null;
        
        // Würfel
        int w = (int)(Math.random() * 6) + 1;
        for (int j = 0; j < players.length; j++) {
            
            if (players[j].getWombat().zug(w)) {
                
                if (fin == null) {
                    fin = players[j].getWombat();
                }
                
                
            }
            //System.out.println(players[j].getWombat());
            
        }
        return fin;
        
    }
    
    
    
}

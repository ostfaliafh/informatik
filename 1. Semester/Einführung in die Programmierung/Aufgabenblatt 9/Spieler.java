/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s9;

/**
 *
 * @author arch
 */
public class Spieler {
    private String colour;
    private Wombat wombat;
    
    
    public Spieler (String colour, Wombat wombat) {
        this.colour = colour;
        this.wombat = wombat;
    }
    
    public String getColouer () {
        return colour;
    }
    
    public Wombat getWombat() {
        return wombat;
    }
    
    public void setColour (String colour) {
        this.colour = colour;
    }
    
    public void setWombat (Wombat wombat) {
        this.wombat = wombat;
    }
    

    
    
}

package de.ostfalia.gdp.ws16.s5;

import java.util.Scanner;

public class BasicStatistics {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		
		/**
		 * run = Durchläufe
		 * aver = Durchschnitt
		 * max = Maximum
		 * min = Minimum
		 * var = Benötigt um Standardabweichung zu berechnen
		 * stde = Standardabweichung
		 * undaver = Anzahl unterm Durchschnitt
		 * ovaver = Anzahl übern Durchschnitt 
		 */
		
		int run;
		double aver = 0, max = 0, min = 0, var = 0, stde, undaver = 0, ovaver = 0;
		double[] input;
		System.out.println("Wie viele Durchläufe");
		run = sc.nextInt();
		input = new double[run];
		
		for (int i = 0; i < run; i++) {
			System.out.println(i + ". Wert: ");
			input[i] = sc.nextDouble();
			aver = aver + input[i];
			if (input[i] == input[0]) {
				min = input[0];
			}
			if (input[i] > max) {
				max = input[i];
			} else if (input[i] < min) {
				min = input[i];
			}
		}
		
		/**
		 * Mathe Foo
		 */
		aver = aver / run;
		for (int i = 0; i < run; i++) {
			var = var + Math.pow((input[i] - aver), 2);
		}
		var = var / run;
		stde = Math.sqrt(var);
		
		for(int i = 0; i < run; i++) {
			if (input[i] < aver) {
				undaver++;
			} else if (input[i] >= aver) {
				ovaver++;
			}
		}
		
		
		System.out.println(aver);
		System.out.println("Auswertung von " + run + " Werten\n"
				+ "Mittelwert: " + aver + "\n"
				+ "Max: " + max + "\n"
				+ "Min: " + min + "\n"
				+ ">= Mittelwert: " + ovaver + "\n"
				+ "< Mittelwert: " + undaver + "\n"
				+ "Standartabweichung: " + stde);
		sc.close();
	}

}

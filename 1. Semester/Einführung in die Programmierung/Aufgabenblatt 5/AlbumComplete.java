package de.ostfalia.gdp.ws16.s5;

import java.util.Scanner;

public class AlbumComplete {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		/**
		 * anz = Anzahl der Karten
		 * buy = Anzahl der Karten die gekauft werden müssen
		 * most = Deck mit der größten Menge von Karten
		 * deck[set][0] = Karten nummern 
		 * deck[set][1] = Jewwilige Anzahl an karten
		 */
		int anz;
		System.out.println("Wie viele Karten gibt es?");
		anz = sc.nextInt();
		calc(anz);
		sc.close();
	}
	
	public static void calc (int set) {
		int buy = 0, most = 0;
		int[][] deck = new int[set][2];
		boolean full = false;
		for (int i = 0; i < set; i++) {
			deck[i][0] = i;
		}
		while (full == false) {
			buy++;
			int card = (int) (Math.random() * set);
			deck[card][1]++;
			for (int i = 0; i < set; i++) {
				if (deck[i][1] == 0) {
					break;
				} else if (i == (set - 1) && deck[i][1] != 0) {
					full = true;
					for (int j = 1; j < set; j++) {
						if (deck[j][1] >= deck[j - 1][1]) {
							most = j;
						}
					}
				}
			}
		}
		System.out.println("Das Deck ist vollständig\n"
				+ "Erworben wurden: " + buy + " Karten\n"
				+ "Karte " + deck[most][0] + " ist einer der Häufigsten, sie ist " + deck[most][1] + "mal vorhanden");
		
	
	}

}

package de.ostfalia.gdp.ws16.s5;

import java.util.Scanner;

public class MinDistance {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		/**
		 * run = Durchläufe
		 * min = Minimum an Karten
		 * inp[run][0] = Eingabe
		 * inp[run][1] = Häufigkeit
		 */
		int run, min = 0;
		int[][] inp;
		System.out.println("Wie viele Zahlen möchtest du eingeben?");
		run = sc.nextInt();
		inp = new int[run][2];
		for (int i = 0; i < run; i++) {
			System.out.println(i + ". Zahl: ");
			inp[i][0] = sc.nextInt();
			if (i > 0 ) {
				if (inp[i - 1][0] >= inp[i][0]) {
					inp[i - 1][1] = inp[i - 1][0] - inp[i][0];			
				} else {
					inp[i - 1][1] = inp[i][0] - inp[i - 1][0];
				}
				//System.out.println("differenz: " + inp[i - 1][1]);
			}
			//System.out.println("Eingabe: " + inp[i][0]);

		}
		for (int j = 1; j < (run -1); j++) {
			if (inp[j][1] < inp[j - 1][1]) {
				min = inp[j][1];
				//System.out.println("minimum: " + min);
			}
		}
		System.out.println("Der minimale Abstand zwischen zwei Nachbarn ist " + min);
		for (int i = 0; i < run; i++) {
			if (inp[i][1] == min) {
				System.out.println("Die " + i + ". und " + (i+1) + ". Zahl haben den Abstand " + min);
			}
		}
		sc.close();
		
	}
}

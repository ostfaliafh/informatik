package de.ostfalia.gdp.ws16.s11;

import java.io.*;
import java.util.Scanner;
/**
 * Author: Daniel Thümen
 * Created by arch on 12/7/16.
 */
public class Main {
    /**
     *
     * @param args
     * @throws IOException
     */
        public static void main(String[] args) throws IOException {
            int inp;
            Scanner sc = new Scanner(System.in);
            boolean check = true;
            //count ist für die Überprüfung damit nicht X Mal irgendein Bullshit eingegeben werden kann....
            int count = 3;
            do {
                String nr = "";
                System.out.println("Bitte zwischen folgenden aufgaben wählen (1 - 5):\n" +
                        "(1) Tabelle aus Datei einlesen\n" +
                        "(2) Tabelle via Handeintrage (Bitte nur bei neuer Saison)\n" +
                        "(3) Neue Stände eintragen\n" +
                        "(4) Tabelle ausgeben\n" +
                        "(5) Neue Tabelle Speichern und Beenden");
                nr = sc.nextLine();
                switch (nr) {
                    case "1":
                        String data = "";
                        FileReader line = null;
                        try {
                            line = new FileReader("/home/arch/workspace/GdPLab/Bundesliga.csv");
                            BufferedReader br = new BufferedReader(line);
                            while ((data = br.readLine()) != null ) {
                                Bundesliga.initTableFromFile(data);
                            }
                            System.out.println("Die Tabelle wurde erfolgreich eingelesen!");
                        } catch (FileNotFoundException e) {
                            System.out.println("Datei nicht gefunden");
                            check = false;
                        } catch (NullPointerException e) {
                            System.out.println("Fehlerhafte Datei");
                            check = false;
                        }
                        count = 3;
                        break;
                    case "2":
                        Scanner sc1 = new Scanner(System.in);
                        String name = "";
                        //Es gibt nur 18 Vereine in der Bundesliga
                        for (int i = 0; i < 18; i++) {
                            System.out.println("Name des Vereins eingeben:");
                            name = sc1.nextLine();
                            Bundesliga.initTable(name);
                        }
                        System.out.println("Alle Vereine wurden erfolgreich eingelesen!");
                        count = 3;
                        break;
                    case "3":
                        String v1, v2;
                        int t1, t2;
                        Scanner sc2 = new Scanner(System.in);
                        System.out.print("Welche Vereine haben gespielt?\n" +
                                "Verein 1: ");
                        v1 = sc2.nextLine();
                        System.out.print("Tore: ");
                        t1 = sc.nextInt();
                        System.out.print("Verein 2: ");
                        v2 = sc2.nextLine();
                        System.out.print("Tore: ");
                        t2 = sc.nextInt();

                        if (Bundesliga.game(v1, v2, t1, t2) == true) {
                            System.out.println("Das Spiel wurde erfolgreich eingetragen");
                        } else {
                            System.out.println("Es muss sich irgendwo ein Fehler eingeschlichen haben");
                        }
                        break;
                    case "4":
                        Bundesliga.table();
                        break;
                    case "5":
                        check = false;
                        System.out.println("Bitte geben Sie einen Namen der Datei, die Dateiendung wird " +
                                "automatisch auf .csv gesetzt");
                        Scanner sc3 = new Scanner(System.in);
                        String save = sc3.nextLine();
                        save += ".csv";
                        Bundesliga.speichern(save);
                        break;

                    default:
                        System.out.println("Bitte eine --> korrekte <-- Eingabe machen, du hast noch " + count +
                                " Versuche");
                        count--;
                        if (count == 0) {
                            check = false;
                        }
                        break;


                }
            } while (check == true);
    }
}

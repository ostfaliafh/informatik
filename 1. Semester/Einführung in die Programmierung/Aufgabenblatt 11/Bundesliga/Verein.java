package de.ostfalia.gdp.ws16.s11;

/**
 * Created by arch on 12/7/16.
 */
public class Verein {
    private String name;
    private int games;
    private int win;
    private int draw;
    private int lose;
    private int goal;
    private int enemygoal;
    private int goaldif;
    private int points;

    @Override
    public String toString() {
        return name + " | " + games + " | " + win + " | " + draw + " | " + lose + " | " + goal + " | " +
                enemygoal + " | " + goaldif + " | " + points;
    }

    /**
     * Konstruktor für Händische eingabe
     * @param name
     */
    public Verein(String name) {
        this(name, 0, 0, 0, 0, 0,
                0, 0, 0);
    }

    /**
     * Gesamt Konstruktor
     * @param name
     * @param games
     * @param win
     * @param draw
     * @param lose
     * @param goal
     * @param enemygoal
     * @param goaldif
     * @param points
     */
    public Verein(String name, int games, int win, int draw, int lose, int goal, int enemygoal, int goaldif, int points) {
        this.name = name;
        this.setGames(games);
        this.setWin(win);
        this.setDraw(draw);
        this.setLose(lose);
        this.setGoal(goal);
        this.setEnemygoal(enemygoal);
        this.setGoaldif(goaldif);
        this.setPoints(points);
    }

    /**
     * Gibt Anzahl der Spiele zurück
     * @return
     */
    public int getGames() {
        return games;
    }

    /**
     * Liest anzahl der Spiele ein und überprüft auf Korrektheit
     * @param games
     */
    public void setGames(int games) {
        if (games >= 0) {
            this.games = games;
        } else {
            this.games = 0;
        }
    }

    /**
     * Gibt anzahl der Gewinne aus
     * @return
     */
    public int getWin() {
        return win;
    }

    /**
     * Liest anzahl der Gewinne ein und überprüft auf Korrektheit
     * @param win
     */
    public void setWin(int win) {
        if (win >= 0) {
            this.win = win;
        } else {
            this.win = 0;
        }
    }

    /**
     * Gibt anzahl der Untendschiedenen Spiele aus
     * @return
     */
    public int getDraw() {
        return draw;
    }

    /**
     * Liest anzahl der Unentschiedenen Spiele ein und überprüft auf Korrektheit
     * @param draw
     */
    public void setDraw(int draw) {
        if (draw >= 0) {
            this.draw = draw;
        } else {
            this.draw = 0;
        }
    }

    /**
     * Gibt anzahl der Verlorenen spiele aus
     * @return
     */
    public int getLose() {
        return lose;
    }

    /**
     * Liest anzahl der Verlorenen Spiele ein und überprüft auf Korrektheit
     * @param lose
     */
    public void setLose(int lose) {
        if (lose >= 0) {
            this.lose = lose;
        } else {
            this.lose = 0;
        }
    }

    /**
     * Gibt anzahl der Tore aus
     * @return
     */
    public int getGoal() {
        return goal;
    }

    /**
     * Liest anzahl der Tore ein und überprüft auf Korrektheit
     * @param goal
     */
    public void setGoal(int goal) {
        if (goal >= 0) {
            this.goal = goal;
        } else {
            this.goal = 0;
        }
    }

    /**
     * Gibt anzahl der Gegnertore aus
     * @return
     */
    public int getEnemygoal() {
        return enemygoal;
    }

    /**
     * Liest anzahl der Gegnertore ein und überprüft auf Korrektheit
     * @param enemygoal
     */
    public void setEnemygoal(int enemygoal) {
        if (enemygoal >= 0) {
            this.enemygoal = enemygoal;
        } else {
            this.enemygoal = 0;
        }
    }

    /**
     * Gibt die Tordifferenz aus
     * @return
     */
    public int getGoaldif() {
        return goaldif;
    }

    /**
     * Liest anzahl der Tordifferenz
     * @param goaldif
     */
    public void setGoaldif(int goaldif) {
        if ((this.getGoal() - this.getEnemygoal()) == goaldif) {
            this.goaldif = goaldif;
        } else {
            this.goaldif = 0;
        }
    }

    /**
     * Gibt Punkte aus
     * @return
     */
    public int getPoints() {
        return points;
    }

    /**
     * Liest anzahl der Punkte ein
     * @param points
     */
    public void setPoints(int points) {
        if (((this.getWin() * 3) + (this.getDraw() * 1)) == points) {
            this.points = points;
        } else {
            this.points = 0;
        }
    }

    /**
     * Gibt Namen aus
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Liest Namen ein
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Addiert punkte und Tore zu Vereinen die gegeneinander spielen
     * @param b - Gegnerverein
     * @param t1 - Tor des Vereins
     * @param t2 - Tor des Gegnervereins
     * @return
     */
    public Verein spiel(Verein b, int t1, int t2){
        this.setGames(this.getGames() + 1);
        b.setGames(b.getGames() + 1);
        this.setGoal(this.getGoal() + t1);
        b.setGoal(b.getGoal() + t2);
        this.setEnemygoal(this.getEnemygoal() + t2);
        b.setEnemygoal(b.getEnemygoal() + t1);
        this.setGoaldif(this.getGoal() - this.getEnemygoal());
        b.setGoaldif(b.getGoal() - b.getEnemygoal());

        if (t1 > t2) {
            this.setWin(this.getWin() + 1);
            this.setPoints(this.getPoints() + 3);
            b.setLose(b.getLose() + 1);
        } else if (t1 == t2) {
            this.setPoints(this.getPoints() + 1);
            b.setPoints(b.getPoints() + 1);
            this.setDraw(this.getDraw() + 1);
            b.setDraw(b.getDraw() + 1);
        } else {
            b.setWin(b.getWin() + 1);
            b.setPoints(b.getPoints() + 3);
            this.setLose(this.getLose() + 1);
        }
        return this;
    }
}

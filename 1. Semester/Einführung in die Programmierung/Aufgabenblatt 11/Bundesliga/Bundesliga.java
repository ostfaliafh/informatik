package de.ostfalia.gdp.ws16.s11;


import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by arch on 12/7/16.
 */
public class Bundesliga {
    //Es gibit Maximal 18 Vereine in der Bundesliga
    static Verein[] db = new Verein[18];
    private static int count = 0;

    /**
     * Liest die Tabelle aus einer Datei ein
     * @param line - Tabellenzeile
     */
    public static void initTableFromFile(String line) {
        //Auch hier, es gibt nur 18 Vereine in der Bundesliga
        if (count < 18) {
            String[] temp = line.split("[;:]");
            //System.out.println(temp[0]);
            //Verein tmp = new Verein(temp[0]);
            Verein tmp = new Verein(temp[0], Integer.parseInt(temp[1]), Integer.parseInt(temp[2]),
                    Integer.parseInt(temp[3]), Integer.parseInt(temp[4]), Integer.parseInt(temp[5]),
                    Integer.parseInt(temp[6]), Integer.parseInt(temp[7]), Integer.parseInt(temp[8]));
            db[count] = tmp;
            //System.out.println(count + ", " + temp[0] + ", " + temp[1] + ", " + temp[2] + ", " + temp[3] + ", " +
            // temp[4] + ", " + temp[5] + ", " + temp[6] + ", " + temp[7] + ", " + temp[8]);
            count++;
        }
    }

    /**
     * Liest die Tabelle aus, falls die Vereine via Hand eingetragen werden
     * @param name - Vereinsname der übergeben wird
     */
    public static void initTable(String name) {
        Verein tmp = new Verein(name);
        db[count] = tmp;
        count++;
    }

    /**
     * Gibt die Tabelle aus
     */
    public static void table() {
        if (db[0] != null) {

            System.out.println("Verein | Spiele | Gewinne | Verloren | Unentschieden | Tore | Gegn. Tore | " +
                    "Tor Diff. | Pkt.\n" +
                    "-------------------------------------------------------------------------------------------");
            for (int i = 0; i < db.length; i++) {
                System.out.println(db[i].toString());
            }
        } else {
            System.out.println("In der Datenbank befinden sich keine Vereine!\n");
        }
    }

    /**
     * Hier werden die neuen Daten den Vereinen angerechnet
     * @param v1 - Verein 1
     * @param v2 - Verein 2
     * @param t1 - Tore des ersten Vereins
     * @param t2 - Tore des zweitens Vereins
     * @return
     */
    public static Boolean game(String v1, String v2, int t1, int t2) {
        int dbnr1 = 0, dbnr2 = 0, test1 = 0, test2 = 0;
        for (int i = 0; i < db.length; i++) {
            if (v1.equals(db[i].getName())) {
                System.out.println("gefunden 1");
                dbnr1 = i;
            }
            if (v2.equals(db[i].getName())) {
                dbnr2 = i;
                System.out.println("gefunden 2");
            }
        }


        if (dbnr1 != dbnr2) {
            db[dbnr1].spiel(db[dbnr2], t1, t2);

            System.out.println("Verein | Spiele | Gewinne | Verloren | Unentschieden | Tore | Gegn. Tore | " +
                    "Tor Diff. | Pkt.\n" +
                    "-------------------------------------------------------------------------------------------\n" +
                    "" + db[dbnr1].toString() + "\n" +
                    "" + db[dbnr2].toString());
            do{
                test1 = aktualisiere(dbnr1);
            }while (dbnr1 != test1);

            do {
                test2 = aktualisiere(dbnr2);
            }while (dbnr2 != test2);
            System.out.println("Tabelle Aktualisiert");


            return true;
        } else {
            return false;
        }


    }

    /**
     * Diese Methode aktualisiert die Tabelle
     * @param t1 - Dieser Parameter spiegelt den aktuellen Verein wieder der in der in der Tabelle einsortiert wird
     * @return
     */
    public static int aktualisiere(int t1) {
        Verein tmp;
        if ((db[t1].getPoints() >= db[t1 - 1].getPoints()) & t1 > 0) {
            if ((db[t1].getPoints() == db[t1-1].getPoints()) & (db[t1].getGoaldif() > db[t1 - 1].getGoaldif())) {
                tmp = db[t1 -1];
                db[t1 - 1] = db[t1];
                db[t1] = tmp;
                t1 -= 1;
            } else if (db[t1].getPoints() > db[t1-1].getPoints()){
                tmp = db[t1 -1];
                db[t1 - 1] = db[t1];
                db[t1] = tmp;
                t1 -= 1;
            }
        }

        return t1;
    }

    /**
     * Diese Methode sorgt dafür, dass die Datei wie beim Einlesen gespeichert wird.
     * @param name - Übergebener Parameter aus der Main Klasse
     * @throws IOException
     */
    public static void speichern(String name) throws IOException {
        FileWriter out = new FileWriter(name);
        for (int i = 0; i < db.length; i++) {
            out.write(db[i].getName() + ";" + db[i].getGames() + ";" + db[i].getWin() + ";" + db[i].getLose() +
                    ";" + db[i].getDraw() + ";" + db[i].getGoal() + ":" + db[i].getEnemygoal() +
                    ";" + db[i].getGoaldif() + ";" + db[i].getPoints() + "\n");
        }
        out.close();

    }




}

package de.ostfalia.gdp.ws16.s3;

import java.util.Scanner;

public class Loops {
		public static void main (String[] args) {
			Scanner sc = new Scanner (System.in);
			int zahl, i;
			System.out.println("Welche Zahl soll berechnet werden?");
			zahl = sc.nextInt();
			System.out.println("Welche Schleife soll verwendet werden?\n"
					+ "1. While-Loop\n"
					+ "2. Do-While-Loop\n"
					+ "3. For-Loop\n"
					+ "Please choose Number 1 - 3");
			i = sc.nextInt();
			switch (i) {
				case 1: 
					wloop(zahl);
					break;
				case 2:
					dwloop(zahl);
					break;
				case 3:
					floop(zahl);
					break;
				default:
					System.out.println("Falsche eingabe");
			}
			sc.close();
		}
		
		public static void wloop(int a) {
			int quer = 0, gross = 0, sum = a, test = 1;
			while (test > 0) {
				quer = quer + (a % 10);
				test = (a % 10);
				a = a / 10;
				
				if (test > gross && test < 10) {
					gross = test;
				} 
			}
			if (quer % 3 == 0) {
				System.out.println("Von " + sum + " lautet die Quersumme: " + quer + " und die größte Ziffer ist: " + gross + " und ist durch 3 Teilbar");
			} else {
				System.out.println("Von " + sum + " lautet die Quersumme: " + quer + " und die größte Ziffer ist: " + gross + " und ist nicht durch 3 Teilbar");
			}
			
		}
		public static void dwloop(int a) {
			int quer = 0, gross = 0, sum = a, test;
			do {
				quer = quer + (a % 10);
				test = (a % 10);
				a = a / 10;
				
				if (test > gross && test < 10) {
					gross = test;
				} 
			} while (test > 0);
			
			if (quer % 3 == 0) {
				System.out.println("Von " + sum + " lautet die Quersumme: " + quer + " und die größte Ziffer ist: " + gross + " und ist durch 3 Teilbar");
			} else {
				System.out.println("Von " + sum + " lautet die Quersumme: " + quer + " und die größte Ziffer ist: " + gross + " und ist nicht durch 3 Teilbar");
			}
		}
		public static void floop(int a) {
			String laenge = Integer.toString(a);
			int quer = 0, gross = 0, sum = a, test;
			for (int i = 0; i <= laenge.length(); i++) {
				quer = quer + (a % 10);
				test = (a % 10);
				a = a / 10;
				if (test > gross && test < 10) {
					gross = test;
				} 
			}
			if (quer % 3 == 0) {
				System.out.println("Von " + sum + " lautet die Quersumme: " + quer + " und die größte Ziffer ist: " + gross + " und ist durch 3 Teilbar");
			} else {
				System.out.println("Von " + sum + " lautet die Quersumme: " + quer + " und die größte Ziffer ist: " + gross + " und ist nicht durch 3 Teilbar");
			}
		}
}

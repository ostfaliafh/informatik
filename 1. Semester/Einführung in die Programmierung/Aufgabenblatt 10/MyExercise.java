/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s10;

import java.util.Arrays;

/**
 *
 * @author arch
 */
public class MyExercise {
    
    public static void main (String[] args) {
        int[] test = {1,-3,-5,7};
        
        int[] test2 = {1,-3,-5,7,-5,-3,1};
        
        System.out.println(addEvenEntries(test) + "\n"
                + addEvenEntries(test2) + "\n"
                + Arrays.toString(selectIth(test, 2)) + "\n"
                + Arrays.toString(selectIth(test2, 3)));
    }
    
    /**
     * addiert alle Feldelemente mit geradem Index,
     * @param feld - Feld, dessen Elemente addiert werden
     * @return summe - Summe
     * Beispiel: feld={1,-3,-5,7} Ergebnis: -4
     */
    
    public static int addEvenEntries(int[] feld) {
        int ret = 0;
        for (int i = 0; i < feld.length; i += 2) {
            ret += feld[i];
        }
        return ret;
    }
    
    
    /**
     * erzeugt ein neues Feld, in dem jedes i.te Element des alten Feldes enthalten ist
     * @param feld - Feld
     * @param i - Selektor
     * @return efeld - Feld in dem das 0.te, i.te, 2i.te,... Element von Feld vorkommen
     * bei nichtpositiven i wird feld zurückgegeben
     * Beispiel:
     * feld={1,2,3,4,5,6,7,8}, dann selectIth(feld,3) = {1,4,7}
     */
    public static int [] selectIth(int[] feld, int i) {
        String tmp= "";
        for (int j = 0; j < feld.length; j +=3) {
            tmp += feld[j] + "";
        }
        String[] tmp_arr = tmp.split("");
        int[] ret = new int [tmp_arr.length];
        //System.out.println(tmp);
        for (int j = 0; j < ret.length; j++) {
            ret[j] = Integer.parseInt(tmp_arr[j]);
        }

        return ret;
    }
    
}

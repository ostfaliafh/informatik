/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s10;

public class Bestellposten {
    private String name;
    private int menge;
    
    @Override
    public String toString() {
        return name + " " + menge;
    }
    
    public Bestellposten (String name, int menge) {
        this.name = name;
        this.menge = menge;
    }
    
    public String getName() {
        return name;
    }
    
    public int getMenge() {
        return menge;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setMenge(int menge) {
        this.menge = menge;
    }

    
}

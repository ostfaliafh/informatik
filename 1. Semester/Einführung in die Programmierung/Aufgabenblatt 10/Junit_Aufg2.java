package de.ostfalia.gdp.ws16.s10;

import static org.junit.Assert.*;

import org.junit.Test;

public class Junit_Aufg2 {

	@Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testiteele() {
            int[] test = {1,-3,-5,7};
            int[] erg = {1,7};
            
            for (int i = 0; i < erg.length; i++) {
            	assertEquals(erg[i] , MyExercise.selectIth(test, 2)[i]);	
            }
	}
        @Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testrechner() {
            int[] test = {1,-3,-5,7};
            int erg = -4;
            
            assertEquals(erg , MyExercise.addEvenEntries(test));
	}
        @Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testielem() {
            int[] test = {1,2,3,4,5,6,7,8};
            int[] erg = { 1, 4, 7};
            
            for (int i = 0; i < erg.length; i++) {
            	assertEquals(erg[i] , MyExercise.selectIth(test, 3)[i]);	
            }
	}
        @Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testadd2() {
            int[] test = {1,2,3,4,5,6,7,8};
            int erg = 16;
            
            assertEquals(erg , MyExercise.addEvenEntries(test));	
	}
        @Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testele() {
            int[] test = {1,-3,-5,7,-5,-3,1};
            int[] erg = { 1, 7, 1};
            
            for (int i = 0; i < erg.length; i++) {
            	assertEquals(erg[i] , MyExercise.selectIth(test, 3)[i]);	
            }
	}
        @Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testadd3() {
            int[] test = {1,-3,-5,7,-5,-3,1};
            int erg = -8;
            
            assertEquals(erg , MyExercise.addEvenEntries(test));
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s10;

/**
 *
 * @author arch
 */
public class Ware {
    private String name;
    private double price;
    private int number;

    @Override
    public String toString() {
        return name + " " + price + " " + number;
    }
    
    public String getName() {
        return name;
    }
    
    public double getPrice() {
        return price;
    }
    
    public int getNumber() {
        return number;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public void setNumber(int number) {
        this.number = number;
    }
    
    public Ware (String name, double price, int number) {
        this.name = name;
        this.price = price;
        this.number = number;
    }
    
}

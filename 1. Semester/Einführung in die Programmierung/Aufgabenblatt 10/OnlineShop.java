/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s10;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class OnlineShop {
    static int count = 0;
    static Ware[] db = new Ware[100];
    static int MAX_POSTEN = 10;
    static Bestellposten[] letzteBestellung = new Bestellposten[10];
    static int lastcount = 0;
    
    
    /**
     * Läd den Warenbestand aus einer csv-Datei.
     * csv-Zeile: Warenname, Preis, Bestand
     * Beispiel : Ball, 4.99, 25
     * @param shop - Namen der csv-Datei.
     * @throws IOException
     */
    public static void fuelleLager(String shop) throws IOException {
        boolean check = false;
        
        String[] list = shop.split(", ");
        //System.out.println(list[0]);
        //Ware test = new Ware(list[0], Double.parseDouble(list[1]), Integer.parseInt(list[2]));
        Ware test = new Ware(list[0], Double.parseDouble(list[1]), Integer.parseInt(list[2]));
        //System.out.println(test.getName());
        for (int i = 0; i < count; i++) {
            if (test.getName().equals(db[i].getName())) {
                db[i].setNumber(db[i].getNumber() + test.getNumber());
                check = true;
            }
        }
        if (check == false) {
            db[count] = test;
            count++;
        }
    }
    
    
    /**
     * Bestellung der Ware "warenName" in der gewünschte Menge "anzahl"
     * @param warenName - Name der Ware.
     * @param anzahl - gewünschte Menge
     * @return Preis für die gelieferte Warenmenge.
     */
    public static double bestellenEinzeln(String warenName, int anzahl) {
        double price = 0;
        
        for (int i = 0; i < count; i++){
            if (warenName.equals(db[i].getName())) {
                if (anzahl <= db[i].getNumber()) {
                    price = anzahl * db[i].getPrice();
                    db[i].setNumber(db[i].getNumber() - anzahl);
                } else {
                    price = db[i].getNumber() * db[i].getPrice();
                    Bestellposten tmp = new Bestellposten(warenName, anzahl - db[i].getNumber());
                    letzteBestellung[lastcount] = tmp;
                    lastcount++;
                    db[i].setNumber(0);
                    
                }
            }
        }
        return price;
    }
    
    /**
     * Ausgabe des vorhandenen Lagerbestands auf der Konsole
     * Beispiel:
     * 0: Artikel: Ball, Preis: 4.99, Bestand: 25
     * 1: Artikel: Seil, Preis: 10.99, Bestand: 102 
     */
    public static void bestandsausgabe() {
        for (int i = 0; i < count; i++) {
            System.out.println(db[i]);
        }
    }
    
    public static Bestellposten[] erstelleBestellung() {
        Bestellposten[] buy = new Bestellposten[MAX_POSTEN];
        
        bestellen(buy);
        
        
        return buy;
        
    }
    
    public static double bestellen (Bestellposten[] b) {
        Scanner sc = new Scanner (System.in);
        String name = "";
        int anz, price = 0;
        
        for (int i = 0; i < MAX_POSTEN; i++) {
            System.out.print("Ware Nummer " + (i + 1) + "\n"
                    + "Name der Ware (NULL für frühzeitig Beenden):");
            name = sc.nextLine();
            if (name.equals("NULL")) {
                break;
            }
            System.out.print("Anzahl:");
            anz = sc.nextInt();
            price += bestellenEinzeln(name, anz);
            
        }
        return price;
    }
    
    
    
    
    public static void main (String[] args) throws IOException {
        /**
         * Ware Einlesen
         */
        String shop = "";
        FileReader inp = null;
        try {
            inp = new FileReader("shop.csv");
        } catch (FileNotFoundException e){
            System.out.println("Datei nicht gefunden");
        }
        BufferedReader br = new BufferedReader(inp);
        while ((shop = br.readLine()) != null ) {
            OnlineShop.fuelleLager(shop);
        }
        //for (int i = 0; i < OnlineShop.db.length; i++) {
        //    System.out.println(OnlineShop.db[i]);
        //}
        
        /**
         * Anzeigen Menü
         */
        boolean check = true;
        int wahl;
        Scanner sc = new Scanner(System.in);
        String dbname = "";
        
        do {
            System.out.println("Herzlich Willkommen im Shop XYZ\n"
                + "Bitte wählen Sie eine der folgenden Aktionen aus:\n"
                + "(1) Einzelne Ware Bestellen\n"
                + "(2) Warenbestand Anzeigen\n"
                + "(3) Beenden\n"
                + "(4) Mehrer Waren bestellen\n"
                + "(5) Lager Auffüllen");
            wahl = sc.nextInt();
            switch (wahl) {
                case 1: 
                    Scanner sc1 = new Scanner(System.in);
                    System.out.println();
                    String name;
                    int anz;
                    System.out.println("Name der Ware:");
                    name = sc1.nextLine();
                    System.out.println("Anzahl:");
                    anz = sc.nextInt();
                    System.out.println(bestellenEinzeln(name, anz));
                    break;
                case 2:
                    System.out.println();
                    bestandsausgabe();
                    break;
                case 3:
                    check = false;
                    break;
                case 4: 
                    erstelleBestellung();
                case 5:
                    Scanner sc2 = new Scanner(System.in);
                    System.out.print("Name der Datenbank: ");
                    dbname = sc2.nextLine();
                    try {
                        inp = new FileReader(dbname);
                    } catch (FileNotFoundException e){
                        System.out.println("Datei nicht gefunden");
                    }
                    br = new BufferedReader(inp);
                    while ((shop = br.readLine()) != null ) {
                        OnlineShop.fuelleLager(shop);
                    }
                    while (lastcount > 0) {
                        bestellenEinzeln(letzteBestellung[lastcount - 1].getName(), letzteBestellung[lastcount - 1].getMenge());
                        //System.out.println(lastcount);
                        lastcount--;
                        //System.out.println(lastcount + "\n -------------");
                    }
                    
                default:
                    System.out.println();
            }
        } while (check == true);
        
        
        
    }
    
    
}

package de.ostfalia.gdp.ws16.s10;

import static org.junit.Assert.*;

import org.junit.Test;

public class Junit_Aufg3 {
        
	@Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test negativ
         */
	public void testNegGanz() {
            assertEquals("-6:" , "-6" , new GemBruch(-6).toString());		
            assertEquals("-7:" , "-7" , new GemBruch(-7).toString());
	}
        
        @Test (timeout = 1000)
        /**
         * Ganzzahliger Konstruktor test Positiv
         */
	public void testPosGanz() {
            assertEquals("6:" , "6" , new GemBruch(6).toString());		
            assertEquals("7:" , "7" , new GemBruch(7).toString());
	}
        
        @Test (timeout = 1000)
        /**
         * Zähler/Nenner Konstruktor test negativ
         */
	public void testNegNenZa() {
            assertEquals("-3/2:", "-1_-1/2", new GemBruch(-3, 2).toString());
	}
        
        @Test (timeout = 1000)
        /**
         * Zähler/Nenner Konstruktor test Positiv
         */
	public void testPosNenZa() {
            assertEquals("1/2:", "1/2", new GemBruch(1, 2).toString());
            assertEquals("3/2:", "1_1/2", new GemBruch(3, 2).toString());
	}
        
        @Test
        /**
         * Zähler/Nenner Addition
         */
	public void testadd() {
        	String erg = "1_1/2";
        	String test = new GemBruch(1,2).add(new GemBruch(2,2)).toString();
            assertEquals("1/2 + 2/2:", erg, test);
	}
        
        @Test (timeout = 1000)
        /**
         * Addition von ganzzahligen Brüchen
         */
	public void testaddganz() {
            assertEquals("3 + 4:", "7", new GemBruch(3).add(new GemBruch(4)).toString());			
	
	}
        
        @Test (timeout = 1000)
        /**
         * Zähler/Nenner Substraktion
         */
	public void testsub() {
        	String erg = "1/2";
        	String test = new GemBruch(2,2).sub(new GemBruch(1,2)).toString();
            assertEquals("2/2 - 1/2:", erg, test);
	}
        
        @Test (timeout = 1000)
        /**
         * Subtraktion von ganzzahligen Brüchen
         */
	public void testsubganz() {
            assertEquals("4 - 3:", "1", new GemBruch(4).sub(new GemBruch(3)).toString());			
	
	}
        
        
        
        
}

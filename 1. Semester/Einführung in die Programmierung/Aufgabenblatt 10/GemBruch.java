/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s10;


/**
 *
 * @author arch
 */
public class GemBruch {
    private int ganz;
    private int zaehler;
    private int nenner;

    
    @Override
    public String toString () {
        if (zaehler == nenner) {
            return "" + ganz;
        } else if (ganz == 0) {
            if (zaehler > nenner) {
                return (ganz + (zaehler / nenner))+ "_" + (zaehler % nenner) + "/" + nenner;
            } else if ((zaehler < nenner) & (zaehler < 0)) {
                return (ganz - ((zaehler / nenner) * -1))+ "_" + ((zaehler % nenner)) + "/" + nenner;
            } else {
                return zaehler + "/" + nenner;
            }
        } else {
            return ganz + "_" + zaehler + "/" + nenner;
        }
    }

    public GemBruch(int ganz, int zaehler, int nenner) {
        this.ganz = ganz;
        this.nenner = nenner;
        this.zaehler = zaehler;
    }

    public GemBruch(int zaehler, int nenner) {
        this(0, zaehler, nenner);
    }

    public GemBruch(int ganz) {
        this(ganz, 0, 0);
    }
    
    public int getNenner() {
        return nenner;
    }
    
    public int getGanz() {
        return ganz;
    }
    
    public int getZaehler() {
        return zaehler;
    }
    
    public void setNenner (int nenner) {
        this.nenner = nenner;
    }
    
    public void setZaehler ( int zaehler) {
        this.zaehler = zaehler;
    }
    
    public void setGanz(int ganz) {
        this.ganz = ganz;
    }
    
    public GemBruch add (GemBruch b) {
        GemBruch result = new GemBruch((ganz + b.ganz),(zaehler * b.nenner + b.zaehler * nenner),(nenner * b.nenner));
        if ((zaehler | nenner) != 0) {
            kuerzen(result);
        }
        return result;
    }
    
    public GemBruch sub (GemBruch b) {
        GemBruch result = new GemBruch((ganz - b.ganz),(zaehler * b.nenner - b.zaehler * nenner),(nenner * b.nenner));
        if ((zaehler | nenner) != 0) {
            kuerzen(result);
        }
        return result;
    }
    
    public GemBruch mul (GemBruch b) {
        GemBruch result = new GemBruch((ganz * b.ganz),(zaehler * b.zaehler),(nenner * b.nenner));
        if ((zaehler | nenner) != 0) {
            kuerzen(result);
        }
        return result;
    }
    
    public GemBruch div (GemBruch b) {
        GemBruch result = new GemBruch((ganz / b.ganz),(zaehler * b.nenner),(nenner * b.zaehler));
        if ((zaehler | nenner) != 0) {
            kuerzen(result);
        }
        return result;
    }
    
    private int Ggt (int a, int b) {
        int rest = 0;
        
        if (a < 0) {
            a *= -1; 
        }
        do {
            rest = a % b;
            System.out.println(a + " % " + b + " = " + rest);
            a = b;
            b = rest;
        } while (rest != 0);
        return a;
    }
    
    public void kuerzen(GemBruch r) {
        int k = Ggt(r.getZaehler(),r.getNenner());
        r.setZaehler(r.getZaehler() / k);
        r.setNenner(r.getNenner() / k);
        
        if (r.getZaehler() >= r.getNenner()) {
            r.setGanz(r.getGanz() + (r.getZaehler() / r.getNenner()));
            r.setZaehler((r.getZaehler() % r.getNenner()));
            
        }
        
        if (r.getZaehler() == 0 & r.getNenner() == 1) {
            r.setNenner(0);
            
        }
        
    }
    
    
    
}

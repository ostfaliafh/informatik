/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s6;

/**
 *
 * @author arch
 */
public class CollectorsAlbum {
    


    public static void deck (int set, double price, int simu, double pers) {
        int schnitt = 0;
        for (int i = 1; i <= simu; i++) {
            int durchlauf = simulationen(set);
            schnitt += durchlauf;
            double costs = durchlauf * price;
        }
        schnitt = schnitt / simu;
        System.out.println ("Der Spieler benötigt im Durschnitt: " + schnitt + "Karten\n"
                + "Dir durschnittskosten betragen damit: " + Math.round((schnitt * price) * 100.0) / 100.0 + "€\n"
                + "Damit muss jeder Teilnehmer in etwa " + Math.round((schnitt * price / pers) * 100.0) / 100.0 + "€ zahlen");
        
        
        
    }
    public static int simulationen (int set) {
        int most = 0, buy = 0;
        int[][] deck = new int[set][2];
        boolean full = false;
        for (int i = 0; i < set; i++) {
            deck[i][0] = i;
        }
        while (full == false) {
            buy++;
            int card = (int) (Math.random() * set);
            deck[card][1]++;
            for (int i = 0; i < set; i++) {
                if (deck[i][1] == 0) {
                    break;
                } else if (i == (set - 1) && deck[i][1] != 0) {
                    full = true;
                    for (int j = 1; j < set; j++) {
                        if (deck[j][1] >= deck[j - 1][1]) {
                            most = j;
                        }
                    }
                }
            }
        }
        return buy;
    }
}

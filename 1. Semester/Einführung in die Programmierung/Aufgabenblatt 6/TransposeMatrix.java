package de.ostfalia.gdp.ws16.s6;

public class TransposeMatrix {
	public static void main (String[] args) {
		int[][] A = {{1,2,3,5,6,7}, {4,5,6,7,8,9}, {7,8,9,10,11,12},{10,11,12,13,14,15}};
		int[][] B = new int [A[0].length][A.length];
		
		System.out.println(!(6 < 8) == 6 > 3);
		
		for (int i = 0; i < B.length; i++) {
			for (int j = 0; j < B[0].length; j++) {
				B[i][j] = A[j][i];
				System.out.print(B[i][j] + " ");
			}
			System.out.print("\n");
		}
	}

}

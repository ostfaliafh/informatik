package de.ostfalia.gdp.ws16.s6;

import java.util.Scanner;
/**
 * In dieser Klasse soll berechnet werden, wie Kunden möglichst wenig Kleingeld zurück bekommen
 * 
 * cost = Summe die Bazahlt werden muss
 * pay = Was gezahlt wird
 * diff = Differenz
 */
public class Change {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double cost, pay, diff;
        cost = sc.nextDouble();
        pay = sc.nextDouble();
        diff = Math.round((pay - cost) * 100.0 ) / 100.0;
        if (diff < 0) {
            System.out.println("Du hast zu wenig gezahlt");
        } else {
            math(diff);
        }
        
    }
    /**
     * In dieser Methode wird überprüft ob wie der Rückgabewert in welchen Geldstücken Passt
     * @param zahl 
     */
    public static void math (double zahl) {
        double[] money = {500.0, 200.0, 100.0, 50.0, 20.0, 10.0, 5.0, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};
        System.out.println("Sie bekommen " + zahl + "€ zurück, und zwar in Scheinen: ");
        for (int i = 0; i < money.length; i++) {
            if(money[i] <= zahl) {
                //System.out.println(zahl);
                zahl = Math.round(moneyReverse(money[i], zahl) * 100.0 ) / 100.0;
                
            }
        }   
    }
    /**
     * Hier wird überprüft wie oft der Jeweilige Wert in das Geldstück passt und entsprechend abgezogen
     * @param moneyback
     * @param euro
     * @return 
     */
    public static double moneyReverse (double moneyback, double euro) {
        double anz = euro / moneyback;
        int intanz = (int) anz;
        
        System.out.print(intanz + " * " + moneyback + "€, ");
        euro -= (intanz * moneyback);
        //System.out.println("resteuro" + euro);
        return euro;
    }
}

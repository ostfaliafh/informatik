/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ostfalia.gdp.ws16.s6;

/**
 *
 * @author arch
 */
public class Suchsel {
    public static void main(String[] args) {
        char[][] suchsel = {
				{ 'M', 'Y', 'C', 'F', 'F', 'G', 'M', 'U', 'V', 'L' },
				{ 'X', 'E', 'F', 'W', 'M', 'B', 'S', 'V', 'X', 'K' },
				{ 'I', 'A', 'Q', 'E', 'D', 'C', 'X', 'U', 'R', 'X' },
				{ 'B', 'I', 'S', 'M', 'V', 'S', 'L', 'W', 'A', 'I' },
				{ 'G', 'P', 'N', 'N', 'U', 'Z', 'T', 'R', 'I', 'S' },
				{ 'Y', 'F', 'Q', 'F', 'U', 'D', 'E', 'F', 'Y', 'D' },
				{ 'E', 'J', 'J', 'J', 'O', 'M', 'S', 'V', 'I', 'I' },
				{ 'L', 'X', 'E', 'F', 'E', 'R', 'I', 'J', 'U', 'B' },
				{ 'G', 'U', 'K', 'H', 'H', 'J', 'M', 'J', 'C', 'D' },
				{ 'L', 'U', 'J', 'A', 'V', 'A', 'N', 'A', 'N', 'K' }};
		
        String wort1 = "JAVA";
        String wort2 = "SETLX";
        String wort3 = "INFORM";
        
        //System.out.println(searchForWord(suchsel, wort1));
        //System.out.println(searchForWord(suchsel, wort2));
        System.out.println(searchForWord(suchsel, wort3));
                
    }
    
    public static String searchForWord(char[][] suchsel, String Word) {
        char[] w = Word.toCharArray();
        String ausgabe = Word + " : ";
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                //System.out.print(suchsel[i][j]);
                //Unten nach oben
                if (i > w.length) {
                    if (suchsel[i][j] == w[0]) {
                        for (int k = 1; k < w.length; k++) {
                            if (suchsel[i - k][j] == w[k]) {
                                if (k == (w.length - 1)) {
                                    ausgabe += i + "" + j + " - " + (i - k) + "" + j;
                                }
                            }
                        }
                    }
                }
                //Oben nach unten
                if ((i + w.length) <= 10) {
                    if (suchsel[i][j] == w[0]) {
                        for (int k = 1; k < w.length; k++) {
                            if (suchsel[i + k][j] == w[k]) {
                                if (k == (w.length - 1)) {
                                    ausgabe += i + "" + j + " - " + (i + k) + "" + j;
                                }
                            }
                        }
                    }
                }

                //links nach rechts
                
                if ((j + w.length) <= 10) {
                    if (suchsel[i][j] == w[0]) {
                        for (int k = 1; k < w.length; k++) {
                            if (suchsel[i][j + k] == w[k]) {
                                if (k == (w.length - 1)) {
                                    ausgabe += i + "" + j + " - " + i + "" + (j + k);
                                }
                            }
                        }
                    }
                }
                //rechts nach links
                
                if (j > w.length) {
                    if (suchsel[i][j] == w[0]) {
                        for (int k = 1; k < w.length; k++) {
                            if (suchsel[i][j - k] == w[k]) {
                                if (k == (w.length - 1)) {
                                    ausgabe += i + "" + j + " - " + i + "" + (j - k);
                                }
                            }
                        }
                    }
                }
                //diagonal links nach rechts
                
                if ((i + w.length) <= 10 &  (j + w.length) <= 10) {
                    if (suchsel[i][j] == w[0]) {
                        for (int k = 1; k < w.length; k++) {
                            if (suchsel[i + k][j + k] == w[k]) {
                                if (k == (w.length - 1)) {
                                    ausgabe += i + "" + j + " - " + (i + k) + "" + (j + k);
                                }
                            }
                        }
                    }
                }
                
                //diagonal rechts nach links
                if ((i > w.length) &  (j > w.length)) {
                    if (suchsel[i][j] == w[0]) {
                        for (int k = 1; k < w.length; k++) {
                            if (suchsel[i - k][j - k] == w[k]) {
                                if (k == (w.length - 1)) {
                                    ausgabe += i + "" + j + " - " + (i - k) + "" + (j - k);
                                }
                                
                            }
                        }
                    }
                }				
            }
        }
        System.out.print("\n");
        return ausgabe;
    }
    
}

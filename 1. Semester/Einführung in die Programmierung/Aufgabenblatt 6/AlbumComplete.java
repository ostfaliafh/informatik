package de.ostfalia.gdp.ws16.s6;

import java.util.Scanner;

public class AlbumComplete {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int anz, sammler, simu;
                double price;
		System.out.println("Wie viele unterschiedliche Karten gibt es in einem Deck?");
		anz = sc.nextInt();
                System.out.println("Wie teuer ist eine Karte?");
                price = sc.nextDouble();
                System.out.println("Wie viele Sammler befinden sich in einem Team?");
                sammler = sc.nextInt();
                System.out.println("Wie viele Simulationen sollen laufen?");
                simu = sc.nextInt();
		sc.close();
                
                CollectorsAlbum.deck(anz, price,simu, sammler);
                
	}

}
